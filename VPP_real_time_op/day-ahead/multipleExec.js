const exec = require('child_process').execSync;
cmd = "python /home/choff/coding/maestria/thesisVPP/day-ahead/Probabilistic_dayAhead.py";

objective = 2;

// print info for pareto image
if(objective == 1) {
    n = 0.5;
    console.log("n=",n);
    console.log("| Funcion Obj | E  |  r | ");
    for (i=0; i<=1; i+=0.001) {
        //console.log(Number(i.toFixed(4)));
        r = Number(i.toFixed(4));
        console.log(exec(cmd+" "+r+" "+n).toString());
    }
}

if(objective == 2) {
    r = 0.03;
    console.log("Obj,ExpectedValue,Risk,N_parameter");
    for (i=0.7; i<=0.95; i+=0.001) {
        //console.log(Number(i.toFixed(4)));
        n = Number(i.toFixed(4));
        console.log(exec(cmd+" "+r+" "+n).toString());
    }
}
