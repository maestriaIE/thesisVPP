# coding: utf-8

import matplotlib.pyplot as plt

import cvxpy as cvx
import numpy as np

# costos de energia (se conocen por adelantado)
C = np.array([
    97.47, 96.25, 93.38, 90.86, 93.73, 97.80, 97.99, 103.55, 105.79, 108.95,
    113.45, 112.49, 112.60, 112.58, 112.74, 112.36, 109.04, 129.91, 126.08,
    120.40, 109.52, 105.72, 102.56, 102.56
])

# Potencia en los paneles (se conoce por adelantado) (kW)
P_pv = np.array([
    0, 0, 0, 0, 0, 0, 160, 200, 240, 280, 320, 380, 380, 360, 320, 280, 200,
    80, 0, 0, 0, 0, 0, 0
])

# coeficientes costos diesel: 1.38271604e-5*X^2 +0.228045x + 12.125
alpha_d = [1.38271604e-5]
beta_d = [0.228045]
c = [12.125]

# Potencia maxima de baterias (kW)
Ps_max = 50

# Potencia maxima generador diesel (kW)
Pd_max = 140

# Energia fija en las baterias ¿unidades?
# Es_fijo = 441000
Es_fijo = 120

# Energia maxima de las baterias
# Es_max = 882000
Es_max = 245

# Costo en pesos galon diesel
CgalonDiesel = 8600
ClitroDiesel = 2271.87

# ClitroDiesel = 0

# numero de decimales
nd = 4

# matriz de coeficientes usada para actualizar  deltaE_s
A = np.array([[
    -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1
], [1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [
    0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
], [0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [
    0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
], [0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [
    0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
], [0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [
    0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
], [0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0
], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0], [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0
], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0], [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0
], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0], [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0
], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0], [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0
], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1]])

# Variables del modelo
P_vpp = cvx.Variable(24)
P_d = cvx.Variable(24)
P_s = cvx.Variable(24)
E_s = cvx.Variable(24)
deltaE_s = cvx.Variable(24)

states = []
# iterate over hours
for t in range(0, 24):
    cost_diesel = 0
    # iterate over diesel plants
    for k in range(0, 1):
        cost_diesel += (
            alpha_d[k] * P_d[t]**2 + beta_d[k] * P_d[t]) * ClitroDiesel

    constraints = [
        P_d[t] + P_s[t] + P_pv[t] == P_vpp[t],
        abs(P_s[t]) <= Ps_max, P_s[t] == deltaE_s[t], E_s[t] <= Es_max,
        E_s[t] >= 0, P_d[t] <= Pd_max, P_d[t] >= 0
    ]
    cost = (C[t] * P_vpp[t]) - cost_diesel
    states.append(cvx.Problem(cvx.Maximize(cost), constraints))

problem = sum(states)
problem.constraints += [
    E_s[0] == Es_fijo, E_s[23] == Es_fijo, deltaE_s == A * E_s
]

print('Objetive function: ', problem.solve())
# print("status:",problem.status)
# print("P_vpp:\n", P_vpp.value.round(4))
# print("P_d:", P_d.value.round(4))
# print("P_s:\n", P_s.value)
# print("E_s: \n", E_s.value)

plt.style.use('bmh')

x = np.linspace(1, 24, 24)
fig, axes = plt.subplots(nrows=4, ncols=1, sharex=True)
ylabels = ['A', 'B', 'C', 'D']
P = [C, P_vpp.value, P_s.value, P_pv]

title = 'Deterministic model of  Day-Ahead'

fig.suptitle(title, fontsize=20)

print(P_vpp.value)
for yval, ylabel, ax in zip(P, ylabels, axes):
    ax.plot(x, yval.round(nd))
    ax.set_ylabel(ylabel, fontsize=18, rotation=90)
    ax.set_xticks(np.arange(1, 24, 1))

    # fig.text(0.5, 0.05, 'Time(h)', fontsize=18)
axes[3].set_xlabel('Time(h)', fontsize=18)
plt.show()
