# Dependencies
- cvxpy
- matplotlib
- python
- scipy
- numpy

# Files description

## deterministic_day_ahead-py
here you can find a deterministic version of VPP day-ahead compose by diesel power
plants, batteries and photovoltaic generation. in this model stochastic behavior of
solar radiance and energy market prices are treated as deterministic values.

´$ python deterministic_day_ahead.py´

## Probalilistic_dayAhead.py
in this script the dayAhead model for VPP include stochastic behavior of energy
market prices and solar radiance through robust approximation since both solar
radiance and energy market prices follow normal distribution.
the resulting model includes the energy market prices taking into account the
expected value and a penalization factor of risk aversion in objective function.
the solar radiance was modeled through a probabilistic constraint that must be met
by a probability *n* and it is converted to a deterministic constraint due to
quantile function.
the model receive as parameters the penalization factor of risk aversion and the
probability of aceptation of constraint related to solar radiance.

´$ python Probalisitic_dayAhead.py 0.01 0.8´

## input
you must define in params.xlsx the input parameters of model where *"input"* sheet
it is about the mean of spot market price and its variance also mean of solar
radiation in W/m^2 and its variance.
in *"diesel"* sheet id defined the max power
the cost of diesel litre and the coeficient of the cost equation (a,b) for each
diesel power plant.
in *"batteries"* sheet you must define the max Power, max Energy and the fixed energy
for each batery storage system, by last in *"pv"* sheet you define the area of each
photo-voltaic panel

## output
