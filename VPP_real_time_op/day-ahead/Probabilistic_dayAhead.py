# coding: utf-8
#!/usr/bin/python
import time
import os

import matplotlib.pyplot as plt
# from scipy.interpolate import spline
from scipy.stats import norm as scNorm
import cvxpy as cvx
import numpy as np
import pandas as pd

debug = 3

# numero de decimales
nd = 4


def getData(file_name):

    in_df = pd.read_excel(file_name, sheetname='input')
    diesel_df = pd.read_excel(file_name, sheetname='diesel')
    batt_df = pd.read_excel(file_name, sheetname='batteries')
    pv_df = pd.read_excel(file_name, sheetname='pv')

    data = {
        'C_med': in_df['Cost_med'].values,
        'C_var': in_df['Cost_var'].values,
        'Rad_med': in_df['Radiacion_med'].values,
        'Rad_var': in_df['Radiacion_var'].values,
        'Pst_max': batt_df['Pst_max'].values,
        'Est_max': batt_df['Est_max'].values,
        'Est_fijo': batt_df['Est_fijo'].values,
        'Est_nodes': batt_df['Node'].values,
        'diesel_cost_a_param': diesel_df['a'].values,
        'diesel_cost_b_param': diesel_df['b'].values,
        'diesel_lt_cost': diesel_df['lt_diesel_cost'].values,
        'Pdiesel_max': diesel_df['Pdiesel_max'],
        'Area_pv': pv_df['AreaPanel_m2'].values
    }

    return data


def getb(Ppv_med, Ppv_var, n):
    bi = np.zeros((len(Ppv_med), len(Ppv_med[0])))
    for i in range(len(Ppv_med)):
        for j in range(len(Ppv_med[0])):
            if Ppv_med[i, j] == 0:
                bi[i, j] = Ppv_med[i, j]
            else:
                bi[i, j] = scNorm.ppf(
                    1 - n, loc=Ppv_med[i, j], scale=Ppv_var[i, j])


#    print(Ppv_med[:, 12])
#    print(Ppv_var[:, 12])
    b = np.ones((1, len(Ppv_med))) @ bi
    return b


def getA(tw=24):
    '''
    tw: entero con el tiempo de programacion del day ahead
    '''
    A = np.zeros((tw, tw))
    for i in range(tw):
        A[i, i] = -1
        A[i, i - 1] = 1
    return A


def solveDayAhead(diesel_cost_a_param,
                  diesel_cost_b_param,
                  diesel_lt_cost,
                  Pdiesel_max,
                  Area_pv,
                  C_med,
                  C_var,
                  Rad_med,
                  Rad_var,
                  Pst_max,
                  Est_fijo,
                  Est_max,
                  riezgo=0.001,
                  n=0.8,
                  tw=24):
    C_dso = 50
    C_dso_d = 1.5
    Npv = len(Area_pv)
    Nst = len(Pst_max)
    Ndiesel = len(Pdiesel_max)

    A = getA(tw)

    Ppv_med = ((Area_pv.reshape(Npv, 1) * np.ones(
        (Npv, tw))) @ np.diag(Rad_med)) / 1000
    Ppv_var = ((Area_pv.reshape(Npv, 1) * np.ones(
        (Npv, tw))) @ np.diag(Rad_var)) / 1000

    Pst_max_model = Pst_max.reshape(Nst, 1) * np.ones(tw)
    Est_max_model = Est_max.reshape(Nst, 1) * np.ones(tw)

    ones_Pdiesel = np.ones(Ndiesel)
    Pdiesel_max_model = np.reshape(Pdiesel_max, (Ndiesel, 1)) * np.ones(tw)

    b = getb(Ppv_med, Ppv_var, n)

    # Variables del modelo
    Pvpp = cvx.Variable(tw)
    Pdiesel = cvx.Variable(Ndiesel, tw)
    Pst = cvx.Variable(Nst, tw)
    Est = cvx.Variable(Nst, tw)

    # fx = C_med * Pvpp - riezgo * (C_var**2 * Pvpp**2) - diesel_lt_cost * (
    #    diesel_cost_a_param * Pdiesel + diesel_cost_b_param * Pdiesel**2)

    dieselCost = cvx.sum_entries(
        (Pdiesel.T * np.diag(diesel_cost_b_param) +
         (Pdiesel.T)**2 * np.diag(diesel_cost_a_param)) *
        np.diag(diesel_lt_cost))

    CostPpv = b.sum() * C_dso

    fx = C_med * Pvpp - riezgo * (
        C_var**2 * Pvpp**2) - C_dso_d * dieselCost - CostPpv

    constraints = [
        Pvpp - (np.ones(Nst) * Pst).T - (ones_Pdiesel * Pdiesel).T <= b.T,
        cvx.abs(Pst) <= Pst_max_model, Pst.T == A * Est.T,
        Est <= Est_max_model, Est >= 0, Est[:, 0] == Est_fijo,
        Est[:, tw - 1] == Est_fijo, Pdiesel <= Pdiesel_max_model, Pdiesel >= 0
    ]
    obj = cvx.Maximize(fx)
    problem = cvx.Problem(obj, constraints)
    fobj_value = problem.solve(solver='MOSEK')
    data = {
        'Fobj': fobj_value,
        'Pvpp': Pvpp.value,
        'Pdiesel': Pdiesel.value,
        'Pst': Pst.value,
        'Ppv': b,
        'totalPst': np.ones((1, Nst)) * Pst.value,
        'totalPdiesel': np.ones((1, Ndiesel)) * Pdiesel.value,
        'Est': Est.value,
        'C_med': C_med
    }
    tmp_data_results = {
        'Pvpp_gen': [np.sum(Pvpp.value)],
        'diesel_gen': [np.sum(Pdiesel.value)],
        'Batt_gen': [np.sum(Pst.value)],
        'Ppv_gen': [b.sum()],
        'dieselCost': [C_dso_d * dieselCost.value],
        'Ppv_cost': [CostPpv],
        'Pvpp_cost_med': [(C_med * Pvpp).value],
        'rho': [riezgo],
        'riezgo': [(riezgo * (C_var**2 * Pvpp**2)).value],
        'Fobj:': [problem.value]
    }
    tmp_df = pd.DataFrame(tmp_data_results)
    tmp_df.to_csv('./../document/images/sources/results_sc4.csv', index=False)
    print('***************** DAY-AHEAD *****************')
    print('Pvpp gen  :', np.sum(Pvpp.value))
    print('diesel gen:', np.sum(Pdiesel.value))
    print('Batt gen  :', np.sum(Pst.value))
    print('Ppv gen   :', b.sum(), '\n')
    print('dieselCost:', C_dso_d * dieselCost.value)
    print('Ppv_cost:', CostPpv)
    print('Pvpp cost_med', (C_med * Pvpp).value)
    print('rho:', riezgo)
    print('riezgo:', (riezgo * (C_var**2 * Pvpp**2)).value)
    print('Fobj:', problem.value)
    print('**********************************************')
    return data


def plot(data, ans):
    # print('Fobj:', ans['Fobj'])
    plt.style.use('bmh')

    x = np.linspace(1, 24, 24)
    fig, axes = plt.subplots(nrows=4, ncols=1, sharex=True)
    ylabels = ['Cost_med', 'Pvpp', 'Ps', 'Ppv_med', 'Pdiesel']
    P = [
        data['C_med'], ans['Pvpp'] / 1000, ans['totalPst'].T / 1000,
        ans['Ppv'].T / 1000, ans['totalPdiesel'].T / 1000
    ]

    title = 'Stochastic robust approximation day-ahead γ = 0.001, n = 0.8'
    fig.suptitle(title, fontsize=20)

    for yval, ylabel, ax in zip(P, ylabels, axes):
        ax.plot(x, yval.round(nd))
        ax.set_ylabel(ylabel, fontsize=18, rotation=90)
        ax.set_xticks(np.arange(1, 24, 1))

    axes[3].set_xlabel('Time(h)', fontsize=18)
    plt.show()


def saveData(out_file, ans, tw):
    df_Pvpp = pd.DataFrame(ans['Pvpp'] / 1000, columns=['Pvpp'])
    df_Pst = pd.DataFrame(ans['Pst'].T)
    df_Pdiesel = pd.DataFrame(ans['Pdiesel'].T)
    df_Ppv = pd.DataFrame(ans['Ppv'].T)
    df_Est = pd.DataFrame(ans['Est'].T, columns=ans['Est_nodes'])
    df_totalPst = pd.DataFrame(ans['totalPst'].T)
    df_totalPdiesel = pd.DataFrame(ans['totalPdiesel'].T)
    resume = {
        'Pvpp': np.squeeze(np.asarray(ans['Pvpp'])) / 1000,
        'Ppv': np.squeeze(np.asarray(ans['Ppv'])) / 1000,
        'Pst': np.squeeze(np.asarray(ans['totalPst'])) / 1000,
        'Pdiesel': np.squeeze(np.asarray(ans['totalPdiesel'])) / 1000,
        'Cm': ans['C_med']
    }
    df_all = pd.DataFrame.from_dict(resume)
    df_all = df_all[['Pvpp', 'Ppv', 'Pst', 'Pdiesel', 'Cm']]

    writer = pd.ExcelWriter(out_file, engine='xlsxwriter')
    df_Pvpp.to_excel(writer, sheet_name='Pvpp', index=False)
    df_Pst.to_excel(writer, sheet_name='Pst', index=False)
    df_Pdiesel.to_excel(writer, sheet_name='Pdiesel', index=False)
    df_Ppv.to_excel(writer, sheet_name='Ppv', index=False)
    df_Est.to_excel(writer, sheet_name='Est', index=False)
    df_totalPst.to_excel(writer, sheet_name='total_Pst', index=False)
    df_totalPdiesel.to_excel(writer, sheet_name='total_Pdiesel', index=False)
    df_all.to_excel(writer, sheet_name='day_ahead', index=False)
    writer.save()
    writer.close()

    external_out = 'output/external_data.xlsx'

    writer = pd.ExcelWriter(external_out, engine='xlsxwriter')
    df_Pvpp.to_excel(writer, sheet_name='Pvpp', index=False)
    df_Est.to_excel(writer, sheet_name='Est', index=False)
    cost = np.array(
        [ans['Cost_med'], ans['Cost_var'], ans['Rad_med'], ans['Rad_var']]).T

    df_info = pd.DataFrame(
        cost, columns=['Cost_med', 'Cost_var', 'Rad_med', 'Rad_var'])
    df_info.to_excel(writer, sheet_name='info', index=False)
    writer.save()
    writer.close()

    # write csv for latex image
    resume['time'] = np.arange(1, 25)
    data_values_df = pd.DataFrame(resume)
    print(os.getcwd())
    data_values_df.to_csv(
        './../document/images/sources/day_ahead_sc4.csv', index=False)


def getDayAhead(input_file, output_file, riezgo=0.0001, n=0.8, tw=24):
    '''
    input_file: xlsx input file for dayAhead
    '''
    data = getData(input_file)

    ans = solveDayAhead(
        data['diesel_cost_a_param'], data['diesel_cost_b_param'],
        data['diesel_lt_cost'], data['Pdiesel_max'], data['Area_pv'],
        data['C_med'], data['C_var'], data['Rad_med'], data['Rad_var'],
        data['Pst_max'], data['Est_fijo'], data['Est_max'], riezgo, n, tw)

    ans['Cost_med'] = data['C_med']
    ans['Cost_var'] = data['C_var']
    ans['Rad_med'] = data['Rad_med']
    ans['Rad_var'] = data['Rad_var']
    ans['Est_nodes'] = data['Est_nodes']

    saveData(output_file, ans, tw)
    # plot(data, ans)
    # print('Pvpp_da:', ans['Pvpp'] / 1000)
    return ans['Pvpp'] / 1000, ans['Rad_med'], ans['Rad_var']


if __name__ == "__main__":

    startTime = time.time()
    input_file = './input/dayAhead_params_sc4.xlsx'
    output_file = './output/dayAhead_output_sc4.xlsx'
    getDayAhead(input_file, output_file, 0.0001)
    print(time.time() - startTime)
