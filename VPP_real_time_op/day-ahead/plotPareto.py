# coding: utf-8

import matplotlib.pyplot as plt
import pandas as pd


plt.style.use('bmh')
df = pd.read_csv('variandoGamma.csv')
fig, ax = plt.subplots()
df.plot(x='Risk', y='Expected_Value', ax=ax)
ax.set_xlim([0, 0.2])
ax.set_xlabel('Risk',fontsize=18)
ax.set_ylabel('Expected Value',fontsize=18)
ax.set_title('γ parameter behavior')

df2 = pd.read_csv('variandoN.csv')
fig2, ax2 = plt.subplots()
df2.plot(x='N_parameter', y='Objective_function', ax=ax2)
ax2.set_xlim([0.7,0.9])
ax2.set_xlabel('N parameter',fontsize=18)
ax2.set_ylabel('Objective Function',fontsize=18)
ax2.set_title('n parameter behavior')
plt.show()
