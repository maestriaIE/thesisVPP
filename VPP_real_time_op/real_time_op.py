import numpy as np
import math_model as model
import util
import optimal_sol

testcase = 'input/in35.csv'
data = util.getData(testcase)

N = len(data['lines']) + 1

# input data

# Power profile from day-ahead
Pvpp_da_24 = np.array([
    -1.29872528e-07, 3.80592999e-02, 1.36253688e-02, 7.93381649e-03,
    2.84911188e-02, 3.18884819e-02, 1.34750116e-01, 1.37376530e-01,
    2.28183440e-01, 2.08960810e-01, 3.49797721e-01, 2.86962495e-01,
    3.00545003e-01, 2.89801979e-01, 2.48959733e-01, 2.63150334e-01,
    1.74751187e-01, 6.58920879e-02, 4.15809033e-02, 4.99945995e-02,
    3.32066369e-02, 1.23449583e-02, -5.48328950e-03, -1.61030437e-02
])

# VPP power for next 4 hours
Pvpp_da = Pvpp_da_24[3:7]

# Photovoltaic power for the next 4 hours from 4am to 8am
Ppv = np.array([0, 0, 0.16, 0.2])

V, P, Q, S, C = model.solve(data['Cost'], data['Pgmax'], data['Pgmin'],
                            data['Qgmax'], data['Qgmin'], data['G'], data['B'],
                            N, data['lines'], data['Pd'], data['Qd'],
                            data['Vmin'], data['Vmax'], Pvpp_da, Ppv)

print('Voltages:', V)
print('P:', P)
print('Q:', Q)
optimal_sol.print_err(V)
print('Pgen:', np.sum(P))
