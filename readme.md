Ideas Generales of my VPP
=========================

-   to integate DER and multiple microgrids
-   use convex optimization
-   real time operation

Objectives
==========

General objective
-----------------

To develop an convex optimization model for real time optimal operation
of multiple microgrids with high penetration of distributed energy
resources (e.g renewable energies and energy storage) in order to create
a virtual power plant. This model will consider scenarios with variable
energy prices and an approximated model of the grid.

Specific objectives
-------------------

-   To achieve the state of the art of virtual power plants and convex
    optimization methods applied to electrical systems.
-   To describe the Colombian context around microgrids and virtual
    power plant.
-   To define a convex model for day-ahead scheduled of a market-base
    virtual power plant.
-   To define a convex model for optimal operation of VPP given grid
    technical constraints.
-   To implement efficient algorithm that solve the proposed convex
    model.
-   To perform tests for validate the effectiveness of the proposed
    optimization model.
-   To Analyze the results int the Colombian context.

Work Plan
=========

State of the art \[0/2\]
------------------------

-   \[ \] about VPP
-   \[ \] about Convex optimization (SDP,SOCP)
-   \[ \] about Colombian context

Define a benchmark
------------------

Market base VPP model \[100%\]
------------------------------

-   \[X\] Deterministic model
-   \[X\] Robust optimization model

VPP optimal operation \[%\]
---------------------------

-   \[ \] linealized power flow
-   \[ \] linealized optimal power flow
-   \[ \] convex model with grid constraints

Join day-ahead model with Optimal operation model
-------------------------------------------------

Algorithm solution \[/\]
------------------------

-   \[ \] review of convex models algorithms solutions
-   \[ \] secuencial
-   \[ \] interiot point method alternative
-   \[ \] parallel solution

Conclusions \[/\]
-----------------

-   \[ \] test for all models proposed
-   \[ \] analisis of results

final documents
---------------

-   \[ \] paper for international conference
-   \[ \] paper for international journal type A
-   \[ \] final document for colciencias
-   \[X\] final document of thesis
