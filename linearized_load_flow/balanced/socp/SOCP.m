clc
clear all

load('Datos.mat');

Sistema = Feeder;

NL = length(Sistema(:,1));  % number of lines
N = NL + 1;                 % number of nodes
Ybus  = zeros(N);           % initialize Ybus
SP = zeros(N,1);            % vector to save constant power loads
SZ = zeros(N,1);            % to save constant impedance loads
SI = zeros(N,1);            % to save constat current loads
Mi = zeros(NL,N);
Z = zeros(NL,1);
for k = 1:NL                % calculate the Ybus and separate loads
     N1 = Sistema(k,1);     % node send
     N2 = Sistema(k,2);     % node receive
     ykm = 1/(Sistema(k,3)+j*Sistema(k,4));
     bkm = j*Sistema(k,5);
     Ybus(N1,N1) = Ybus(N1,N1) + ykm + bkm;
     Ybus(N1,N2) = Ybus(N1,N2) - ykm;
     Ybus(N2,N1) = Ybus(N2,N1) - ykm;
     Ybus(N2,N2) = Ybus(N2,N2) + ykm + bkm;
     alfa = Sistema(k,9);
     S = (Sistema(k,6)+j*Sistema(k,7));
     if alfa  == 0
         SP(N2) = -S;
     end
     if alfa  == 1
         SI(N2) = -S;
     end
     if alfa  == 2
         SZ(N2) = -S;
     end
     % matrix de incidencia
     Mi(k,N1)=1;
     Mi(k,N2)=-1;
     Z(k,1)= (Sistema(k,3)+j*Sistema(k,4));

end


G = real(Ybus);             % matrix conductacia
B = imag(Ybus);             % matrix susceptancia
Y = [G -B; B G];            % matrix admitance nodal of sistem

Pd = real(SP + SI + SZ);
Qd = imag(SP + SI + SZ);

Pgmax = [1    0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0]';
Pgmin = [0    0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]';
Qgmax = [1    0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0]';
Qgmin = [0    0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]';


cvx_solver SDPT3
cvx_begin

    variable W(2*N,2*N)   symmetric;   % W = v*v'  matrix of voltage real and imag
    variable P(N,1);                 % P = vector  of injecting nodal active power
    variable Q(N,1);                 % Q = vector of injecting nodal reactive power

    W(1,1)   == 1;                 % W(1,1)   = voltage  slack
    W(:,N+1) == 0;                 % W(:,N+1) = angle slack
    W(N+1,:) == 0;                 % W(N+1,:) = angle slack

    C = W(1:N, 1:N);                % C = part matrix W  v*v
    D = W(1:N, N+1:2*N);            % D = part matrix W v*u
    E = W(N+1:2*N, 1:N);            % E = part matrix W u*v
    F = W(N+1:2*N, N+1:2*N);        % F = part matrix W u*u

    Lr = diag(G*C);
    Or = diag(G*F);
    Pr = diag(B*D);
    Qr = diag(B*E);

    Li = diag(G*D);
    Oi = diag(G*E);
    Pi = diag(B*C);
    Qi = diag(B*F);

    %para limite termico

    AA = Mi*D*Mi';
    BB = Mi*F*Mi';

    minimize(trace(Y*W));

    subject to

        % Balance de potencia
        Lr + Or + Pr - Qr == P;
        Li - Oi - Pi - Qi == Q;

        % limites de potencias
        P - Pd <= Pgmax;
        Q - Qd <= Qgmax;
        P - Pd >= Pgmin;
        Q - Qd >= Qgmin;

        % Limete de voltajes
         diag(C) + diag(F) <= 1.0609 * ones(N,1);
         diag(C) + diag(F) >= 0.9409 * ones(N,1);

        %limite termico
        diag(AA)+ diag(BB) <= (100*abs(Z)).^2;

%          for i = 1:N
%             for j = 1:N
%                 norm([2*W(i,j);W(i,i) - W(j,j)],2) <= W(i,i) + W(j,j);
%                 norm([2*W(i,j+N);W(i,i) - W(j+N,j+N)],2) <= W(i,i) + W(j+N,j+N);
%                 norm([2*W(i+N,j);W(i+N,i+N) - W(j,j)],2) <= W(i+N,i+N) + W(j,j);
%                 norm([2*W(i+N,j+N);W(i+N,i+N) - W(j+N,j+N)],2) <= W(i+N,i+N) + W(j+N,j+N);
%             end
%          end



%   [W x;x' 1]>=0


      cvx_end
       W;


% recuperacion de la variable original
%
% [v,l] = eig(W);
% landa = l(end);
% phi = v(:,end);
% X = (phi*sqrt(landa))*(sqrt(landa)*phi');
% V = (phi*sqrt(landa))*-1;
% cmp = abs(W - X);
