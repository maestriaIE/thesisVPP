clc
clear all



G = [-1.9231 1.9231;1.9231 -1.9231];             % matrix conductacia
B = [0.3846 -0.3846;-0.3846 0.3846];             % matrix susceptancia
Y = [G -B; B G];            % matrix admitance nodal of sistem

Pd = [1 1];     % active power load (constant power, constant current, constant impedance)
Qd = [0.025 0.010];     % reactive power load (constant power, constant current, constant impedance)

Pgmax = [0.50   0.70  0.90 0.20 ]';
Pgmin = [0   0  0 0 ]';
Qgmax = [0.40  1  0.20 0.30 ]';
Qgmin = [0   0 0 0 ]';

N = 2;
Cos = [2 2 2 2];


cvx_begin sdp

    variable W(2*N,2*N) symmetric;   % W = v*v'  matrix of voltage real and imag
    variable P(N,1);                 % P = vector  of injecting nodal active power
    variable Q(N,1);
    variable Pg(4,1);% Q = vector of injecting nodal reactive power
    variable Qg(4,1) 
      W(1,1)   == 1;                 % W(1,1)   = voltage  slack
      W(:,N+1) == 0;                 % W(:,N+1) = angle slack
      W(N+1,:) == 0;                 % W(N+1,:) = angle slack
     


    C = W(1:N, 1:N);                % C = part matrix W  v*v 
    D = W(1:N, N+1:2*N);            % D = part matrix W v*u
    E = W(N+1:2*N, 1:N);            % E = part matrix W u*v
    F = W(N+1:2*N, N+1:2*N);        % F = part matrix W u*u
    
    Lr = diag(G*C);                 
    Or = diag(G*F);
    Pr = diag(B*D);
    Qr = diag(B*E); 
    
    Li = diag(G*D);
    Oi = diag(G*E);
    Pi = diag(B*C);
    Qi = diag(B*F);
    
    %para limite termico
    
%     AA = Mi*D*Mi';
%     BB = Mi*F*Mi';
        
    minimize(Cos*Pg);
    
    subject to
    
        % Balance de potencia 
        
        Lr + Or + Pr - Qr == -P;
        Li - Oi - Pi - Qi == -Q; 
        
        % limites de potencias 
        
        P == [1 1 0 0;0 0 1 1]*Pg - Pd'
        Q == [1 1 0 0;0 0 1 1]*Qg - Qd'
        
        Pg <= Pgmax
        Qg <= Qgmax
        Pg >= Pgmin
        Qg >= Qgmin
        
%         P - Pd <= Pgmax;
%         Q - Qd <= Qgmax;              
%         P - Pd >= Pgmin;
%         Q - Qd >= Qgmin;    
                       
        % Limete de voltajes 
        
        %diag(C) + diag(F) <= 1.0609 * ones(N,1);
        %diag(C) + diag(F) >= 0.9409 * ones(N,1);
        diag(W) >= 0
        %limite termico
        
%         diag(AA)+ diag(BB) <= (100*abs(Z)).^2;
             
        W >= 0; 
             
     cvx_end
   W;
   
% recuperacion de la variable original 
Pg
P
[v,l] = eig(W);
landa = l(end);
phi = v(:,end);
X = (phi*sqrt(landa))*(sqrt(landa)*phi');
V = (phi*sqrt(landa))*-1
cmp = abs(W - X);
