import cvxpy as cvx
import numpy as np
import util
# from numpy.linalg import norm

# get input data (feeder)
data = util.readData("in")

# get Ybus
Ybus, SP, SI, SZ = util.get_Ybus(data)

G = np.real(Ybus)  # matriz conductancia
B = np.imag(Ybus)  # matriz susceptancia

# nodal admitance matriz
Y = np.array(np.vstack((np.hstack((G, -B)), np.hstack((B, G)))))

# active load power
Pd = np.real(SP + SI + SZ)

# reactive load power
Qd = np.imag(SP + SI + SZ)

Pgmax = np.array([
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 1, 0, 0
])
Pgmin = np.array([
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0
])
Qgmax = np.array([
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 1, 0, 0
])
Qgmin = np.array([
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0
])

# num of nodes
N = len(data) + 1

# cvxpy variables
W = cvx.Symmetric(2 * N)  # W = v*v'
P = cvx.Variable(N)  # nodal active power
Q = cvx.Variable(N)  # nodal reactive power
a = cvx.Parameter(2)
b = cvx.Parameter(2)
c = cvx.Parameter(2)
d = cvx.Parameter(2)

C = W[:N, :N]
D = W[:N, N:]
E = W[N:, :N]
F = W[N:, N:]

Lr = cvx.diag(G @ C)
Or = cvx.diag(G @ F)
Pr = cvx.diag(B @ D)
Qr = cvx.diag(B @ E)
Li = cvx.diag(G @ D)
Oi = cvx.diag(G @ E)
Pi = cvx.diag(B @ C)
Qi = cvx.diag(B @ F)

obj = cvx.Minimize(cvx.trace(Y @ W))

constraints = [
    P == Lr + Or + Pr - Qr, Q == Li - Oi - Pi - Qi, P - Pd <= Pgmax,
    Q - Qd <= Qgmax, P - Pd >= Pgmin, Q - Qd >= Qgmin, W[0, 0] == 1,
    W[:, N] == 0, W[N, :] == 0,
    cvx.diag(C) + cvx.diag(F) <= 1.0609 * np.ones(N),
    cvx.diag(C) + cvx.diag(F) >= 0.9409 * np.ones(N)
]
"""
for i in range(N):
    for j in range(N):
        a[0] == 2 * W[i, j], a[1] == W[i, i] - W[j, j]
        b[0] == 2 * W[i, j + N], b[1] == W[i, i] - W[j + N, j + N]
        c[0] == 2 * W[i + N, j], c[1] == W[i + N, i + N] - W[j, j]
        d[0] == 2 * W[i + N, j + N], d[1] == W[i + N, i + N] - W[j + N, j + N]
        constraints.append(cvx.norm(a) <= W[i, i] + W[j, j])
        constraints.append(cvx.norm(b) <= W[i, i] + W[j + N, j + N])
        constraints.append(cvx.norm(c) <= W[i + N, i + N] + W[j, j])
        constraints.append(cvx.norm(d) <= W[i + N, i + N] + W[j + N, j + N])
"""
problem = cvx.Problem(obj, constraints)
results = problem.solve(solver='SCS')
# results = problem.solve(solver='MOSEK')
print('status:', problem.status)
print('Objective:', results)
# print(W.value)
