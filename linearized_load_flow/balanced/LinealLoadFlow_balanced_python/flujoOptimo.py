#!/usr/bin/python

import cvxpy as cvx
import numpy as np
import util
from numpy.linalg import inv

# get input data (feeder)
data = util.readData("in")

# get Ybus
Ybus, SP, SI, SZ = util.get_Ybus(data)

# get voltage solutions
Vs, M, A = util.loadFlow(Ybus, SP, SI, SZ)

NL = len(data)
GN = np.real(Ybus[1:, 1:])
G0 = np.real(Ybus[0, 1:])
G00 = np.real(Ybus[0, 0])
V0 = 1
Sr_max = 0.9
Si_max = 0.9
delta_max = 0.9
h, = np.nonzero(data[:, 7])
NumG = len(h)
D = np.zeros((NL, NumG))
Ceros = np.zeros((NL, NumG))

for k in range(NumG):
    n = h[k]
    D[n, k] = 1

DD = np.zeros((2 * NL, 2 * NumG))
DD[:NL, :NumG] = -D
DD[:NL, NumG:] = Ceros
DD[NL:, :NumG] = Ceros
DD[NL:, NumG:] = D

W = inv(M).dot(DD)
Wrr = W[:NL, :NumG]
Wii = W[NL:, NumG:]
Wri = W[:NL, NumG:]
Wir = W[NL:, :NumG]
tmp = np.zeros(2 * NL)
tmp[:NL] = -np.real(A)
tmp[NL:] = -np.imag(A)
U = inv(M).dot(tmp)
Ur = U[:NL]
Ui = U[NL:]

# CVX variables
Vr = cvx.Variable(NL, 1)
Vi = cvx.Variable(NL, 1)
Sr = cvx.Variable(NumG)
Si = cvx.Variable(NumG, 1)

# CVX objective functions
# min Vr*GN*Vr + Vi*GN*Vi + 2*Vr*G0*V0 + G00
Obj = cvx.Minimize(
    cvx.quad_form(Vr, GN) + cvx.quad_form(Vi, GN) + 2 * (G0 * Vr) * V0 + G00)

# CVX constraints
constraints = [Vr == Ur + Wrr * Sr + Wri * Si, Vi == Ui + Wir * Sr + Wii * Si]

for i in range(NL):
    constraints.append((1 - Vr[i])**2 + Vi[k]**2 <= delta_max**2)

for i in range(NumG):
    constraints.append(Sr[k] <= Sr_max)

# CVX problem
problem = cvx.Problem(Obj, constraints)
results = problem.solve()

print('Funcion Objetivo', results)
print('Sr', Sr.value)
print('Si', Si.value)
print('Vr', Vr.value)
print('Vi', Vi.value)

# save Ybus and Voltages
# np.savetxt('Ybus.txt',Ybus,'%.4f')
# np.savetxt('Res.txt',Res,'%.4f')
