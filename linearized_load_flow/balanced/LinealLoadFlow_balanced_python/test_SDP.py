import cvxpy as cvx
import numpy as np

A1 = np.array([[1, 0, 1], [0, 3, 7], [1, 7, 5]])
A2 = np.array([[0, 2, 8], [2, 6, 0], [8, 0, 4]])
C = np.array([[1, 2, 3], [2, 9, 0], [3, 0, 7]])

X = cvx.Semidef(3)
f = X[0, 0] + 4 * X[0, 1] + 6 * X[0, 2] + 9 * X[1, 1] + 7 * X[2, 2]

obj = cvx.Minimize(f)
constraints = [
    X[0, 0] + 2 * X[0, 2] + 3 * X[1, 1] + 14 * X[1, 2] + 5 * X[2, 2] == 11,
    4 * X[0, 1] + 16 * X[0, 2] + 6 * X[1, 1] + 4 * X[2, 2] == 19
]
prob = cvx.Problem(obj, constraints)
print(prob.solve(solver='MOSEK'))
print(prob.status)
print(X.value)
