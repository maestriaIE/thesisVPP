import timeit
import cvxpy as cvx
import numpy as np
import util

startTime = timeit.default_timer()

initT = timeit.default_timer()
# get input data (feeder)
data = util.readData("in")

# get Ybus
Ybus, SP, SI, SZ = util.get_Ybus(data)

# G = np.asmatrix(np.real(Ybus))  # matriz conductancia
G = np.real(Ybus)  # matriz conductancia
# print(G.dtype)
# exit()
B = np.imag(Ybus)  # matriz susceptancia

# nodal admitance matriz
Y = np.array(np.vstack((np.hstack((G, -B)), np.hstack((B, G)))))

# np.savetxt('Ybus.txt', Ybus * 1E-3, '%.4f')
# Yup = np.array(np.hstack((G, -B)))
# Ydown = np.array(np.hstack((B, G)))

# active load power
Pd = np.real(SP + SI + SZ)

# reactive load power
Qd = np.imag(SP + SI + SZ)

# print(Pd)
# print(Qd)
# print('Y', np.sum(np.abs(Y)))

Pgmax = np.array([
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 1, 0, 0
])
Pgmin = np.array([
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0
])
Qgmax = np.array([
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 1, 0, 0
])
Qgmin = np.array([
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0
])
endT = timeit.default_timer()
print('get data in:', endT - initT)
# num of nodes
N = len(data) + 1

# symmetric and positive semidefinite
W = cvx.Semidef(2 * N)
# W = cvx.Symmetric(2 * N)
P = cvx.Variable(N)
Q = cvx.Variable(N)

C = W[:N, :N]
D = W[:N, N:]
E = W[N:, :N]
F = W[N:, N:]

Lr = cvx.diag(G @ C)
Or = cvx.diag(G @ F)
Pr = cvx.diag(B @ D)
Qr = cvx.diag(B @ E)
Li = cvx.diag(G @ D)
Oi = cvx.diag(G @ E)
Pi = cvx.diag(B @ C)
Qi = cvx.diag(B @ F)

obj = cvx.Minimize(cvx.trace(Y @ W))
constraints = [
    P == Lr + Or + Pr - Qr, Q == Li - Oi - Pi - Qi, P - Pd <= Pgmax,
    Q - Qd <= Qgmax, P - Pd >= Pgmin, Q - Qd >= Qgmin, W[:, N] == 0,
    W[N, :] == 0, W[0, 0] == 1,
    cvx.diag(C) + cvx.diag(F) <= 1.0609 * np.ones(N),
    cvx.diag(C) + cvx.diag(F) >= 0.9409 * np.ones(N)
]

problem = cvx.Problem(obj, constraints)
# results = problem.solve(solver='MOSEK')
results = problem.solve(solver='SCS')
print('status:', problem.status)
print(results)
np.savetxt('W.txt', W.value)

print('cvx solution:', timeit.default_timer() - endT)
print('total execution time:', timeit.default_timer() - startTime, 's')
# print(np.sum(np.abs(P.value)))
