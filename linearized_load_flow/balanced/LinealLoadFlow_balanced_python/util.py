#!/usr/bin/python
import numpy as np
from numpy.linalg import inv


def readData(filename):
    '''
    Read input data from filename

    Parameters
    ----------
    filename : string
        path of filename with input data

    Returns
    ---------
    list
        list with input data by lines each line is itself a list
    '''
    with open(filename) as f:
        return np.array([
            list(map(float, x.split())) for x in f.read().split('\n')
            if len(x) and x[0].isdigit()
        ])


def get_Ybus(Data):
    '''
    Compute Ybus matriz from system data

    Parameters
    ----------
    Data : list
        2D list with description of system

    Returns
    ----------
    numpy 2D complex array
    '''

    NL = len(Data)  # Number of Lines
    NN = NL + 1  # Number of nodes
    Ybus = np.complex_(np.zeros([NN, NN]))  # initialize YBUS
    SP = np.complex_(np.zeros([NN]))  # to save constant power loads
    SZ = np.complex_(np.zeros([NN]))  # to save constant impedance loads
    SI = np.complex_(np.zeros([NN]))  # to save constant current loads
    #print("NL:",NL,"NN:",NN,"Ybus:",len(Ybus),'x',len(Ybus[0]))
    for k in range(NL):  # compute Ybus
        N1 = int(Data[k][0]) - 1
        N2 = int(Data[k][1]) - 1
        ykm = 1 / complex(Data[k][2], Data[k][3])
        bkm = complex(0, Data[k][4])
        Ybus[N1, N1] += ykm + bkm
        Ybus[N1, N2] -= ykm
        Ybus[N2, N1] -= ykm
        Ybus[N2, N2] += ykm + bkm
        alfa = Data[k][8]
        S = complex(Data[k][5], Data[k][6])
        if alfa == 0:
            SP[N2] = -S
        elif alfa == 1:
            SI[N2] = -S
        elif alfa == 2:
            SZ[N2] = -S

    return Ybus, SP, SI, SZ


def loadFlow(Ybus, SP, SI, SZ):
    NN = len(Ybus)
    NL = NN - 1
    YNS = Ybus[0, 1:]  # (vector of the Ybus related to the slack)
    YNN = Ybus[1:, 1:]  # (vector of the Ybus related to other nodes)

    A = YNS - 2 * np.conj(SP[1:]) - np.conj(SI[1:])
    B = np.diag(np.conj(SP[1:]))
    C = YNN - np.diag(np.conj(SZ[1:]))

    # print('A',len(A),'B',len(B),'x',len(B[0]),'C',len(C))
    MM = np.zeros((2 * (NN - 1), 2 * (NN - 1)))
    MM[:NL, :NL] = np.real(B + C)
    MM[:NL, NL:] = np.imag(B - C)
    MM[NL:, :NL] = np.imag(B + C)
    MM[NL:, NL:] = np.real(C - B)

    tmp = np.zeros(2 * NL)
    tmp[:NL] = -np.real(A)
    tmp[NL:] = -np.imag(A)
    VRI = inv(MM).dot(tmp)
    Vs = np.complex_(np.zeros(NN))
    Vs[0] = 1
    # print('VRI:',len(VRI))
    Vs[1:] = np.complex_(list(map(complex, VRI[:NL], VRI[NL:])))
    return Vs, MM, A


if __name__ == "__main__":
    # get input data
    data = readData("in")

    # get Ybus
    Ybus, SP, SI, SZ = get_Ybus(data)

    # get voltage solutions
    Vs, MM, A = loadFlow(Ybus, SP, SI, SZ)

    # save Ybus and Voltages
    np.savetxt('Ybus.txt', Ybus, '%.4f')
    np.savetxt('Res.txt', Vs, '%.4f')

    print('data saved in Ybus.txt and Res.txt files')
