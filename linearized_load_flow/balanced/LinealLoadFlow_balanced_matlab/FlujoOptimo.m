clc
clear all

%        Node1 - Node2 - rkm   -   xkm -    bkm/2   -   P   -   Q   -   D -   alpha
Feeder = [  1    2     0.0025    0.0026    0.0001    0.0000    0.0000    0      0
            2    3     0.0034    0.0036    0.0001    0.0000    0.0000    0      0
            3    4     0.0040    0.0023    0.0000    0.0850    0.0400    0      2
            4    5     0.0013    0.0008    0.0000    0.0000    0.0000    0      0
            5    6     0.0040    0.0023    0.0000    0.0850    0.0400    0      2
            5    7     0.0022    0.0012    0.0000    0.0000    0.0000    0      0
            7    8     0.0042    0.0013    0.0000    0.0420    0.0210    0      0
            7    9     0.0022    0.0012    0.0000    0.0850    0.0400    1      1
            9    10    0.0038    0.0021    0.0000    0.0420    0.0210    0      0
            10   11    0.0043    0.0025    0.0000    0.1400    0.0700    0      1
            11   12    0.0027    0.0015    0.0000    0.1260    0.0620    0      0
            12   13    0.0027    0.0015    0.0000    0.0000    0.0000    0      0
            13   14    0.0026    0.0008    0.0000    0.0850    0.0400    0      0
            13   15    0.0027    0.0015    0.0000    0.0420    0.0210    0      1
            10   16    0.0068    0.0022    0.0000    0.0000    0.0000    0      0
            16   17    0.0167    0.0054    0.0001    0.0420    0.0210    0      2
            16   18    0.0026    0.0008    0.0000    0.0850    0.0400    0      0
            3   19    0.0031    0.0010    0.0000    0.0420    0.0210    0      0
            19   20    0.0019    0.0011    0.0000    0.0420    0.0210    0      0
            20   21    0.0026    0.0008    0.0000    0.1260    0.0630    0      0
            20   22    0.0037    0.0012    0.0000    0.0420    0.0210    0      1
            2   23    0.0024    0.0014    0.0000    0.0850    0.0400    0      0
            23   24    0.0035    0.0020    0.0000    0.0000    0.0000    0      0
            24   25    0.0010    0.0003    0.0000    0.0380    0.0180    0      1
            25   26    0.0068    0.0022    0.0000    0.0850    0.0400    0      2
            24   27    0.0054    0.0031    0.0000    0.0850    0.0400    0      0
            27   28    0.0040    0.0023    0.0000    0.0000    0.0000    0      0
            28   29    0.0037    0.0012    0.0000    0.0420    0.0210    0      0
            27   30    0.0120    0.0039    0.0000    0.0000    0.0000    0      0
            30   31    0.0016    0.0005    0.0000    0.1610    0.0800    0      1
            30   32    0.0099    0.0032    0.0000    0.0420    0.0210    0      2
            2   33    0.0052    0.0017    0.0000    0.0000    0.0000    1      0
            33   34    0.0031    0.0010    0.0000    0.0850    0.0400    0      0
            33   35    0.0042    0.0013    0.0000    0.0930    0.0440    1      2];
                                %
Sistema = Feeder;

NL = length(Sistema(:,1));  % number of lines
NN = NL + 1;                % number of nodes
Ybus  = zeros(NN);          % initialize Ybus
SP = zeros(NN,1);           % vector to save constant power loads
SZ = zeros(NN,1);           % to save constant impedance loads
SI = zeros(NN,1);           % to save constat current loads
for k = 1:NL                % calculate the Ybus and separate loads
  N1 = Sistema(k,1);     % node send
  N2 = Sistema(k,2);     % node receive
  ykm = 1/(Sistema(k,3)+j*Sistema(k,4));
  bkm = j*Sistema(k,5);
  Ybus(N1,N1) = Ybus(N1,N1) + ykm + bkm;
  Ybus(N1,N2) = Ybus(N1,N2) - ykm;
  Ybus(N2,N1) = Ybus(N2,N1) - ykm;
  Ybus(N2,N2) = Ybus(N2,N2) + ykm + bkm;
  alfa = Sistema(k,9);
  S = (Sistema(k,6)+j*Sistema(k,7));
  if alfa  == 0
    SP(N2) = -S;
  end
  if alfa  == 1
    SI(N2) = -S;
  end
  if alfa  == 2
    SZ(N2) = -S;
  end
end
kN = 2:NN;  % all nodes except the slack (slack = 1)
YNS = Ybus(kN,1);   % (vector of the Ybus related to the slack)
YNN = Ybus(kN,kN);  % (vector of the Ybus related to other nodes)
A = YNS-2*conj(SP(kN))-conj(SI(kN)); % matrix A (same as in the paper)
B = diag(conj(SP(kN)));              % matrix B (same as in the paper)
C = YNN-diag(conj(SZ(kN)));          % matrix C (same as in the paper)
MM = [real(B+C),imag(B-C);imag(B+C),real(C-B)]; % (same as in Eq 10 in the paper)
VRI = inv(MM)*[-real(A);-imag(A)];   % voltages (real and imaginary part)

M = MM;
GN = real(YNN);
G0 = real(YNS);
G00 = real(Ybus(1,1));
V0 = 1;
Sr_max = 0.9;
Si_max = 0.9;
delta_max = 0.3;
h = find(Sistema(:,8));
NumG = length(h);
D = zeros(NL,NumG);
Ceros = zeros(NL,NumG);

for k = 1: NumG
  n = h(k);
  D(n,k)=1;
end
DD = [-D Ceros;Ceros D];
W = M^-1*DD;
Wrr = W(1:NL,1:NumG);
Wii = W(NL+1:2*NL,NumG+1:2*NumG);
Wri = W(1:NL,NumG+1:2*NumG);
Wir = W(NL+1:2*NL,1:NumG);
U  = M^-1 *[-real(A);-imag(A)];
Ur = U(1:size(-real(A),1));
Ui = U(size(-real(A),1)+1:size(U,1));


cvx_begin

variable Vr(NL);
variable Vi(NL);
variable Sr(NumG) ;
variable Si(NumG) ;

minimize (Vr'*GN*Vr + Vi'*GN*Vi + 2*Vr'*G0*V0+ G00);

subject to
Vr == Ur + Wrr*Sr + Wri*Si;
Vi == Ui + Wir*Sr + Wii*Si;
for k = 1:NL
  (1-Vr(k))^2 + Vi(k)^2 <= delta_max^2;
end
for k= 1:NumG
  Sr(k) <= Sr_max;
end
cvx_end

Sr
Si
Vr
Vi
