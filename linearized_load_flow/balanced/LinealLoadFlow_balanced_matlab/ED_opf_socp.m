clc
clear all

% 
% %       Node1 - Node2 - rkm   -   xkm -    bkm/2   -   P   -   Q   -   D -   alpha
% Feeder = [  1    2     0.0025    0.0026    0.0001    0.0000    0.0000    0      0
%             2    3     0.0034    0.0036    0.0001    0.0000    0.0000    0      0
%             3    4     0.0040    0.0023    0.0000    0.0850    0.0400    0      2
%             4    5     0.0013    0.0008    0.0000    0.0000    0.0000    0      0
%             5    6     0.0040    0.0023    0.0000    0.0850    0.0400    0      2
%             5    7     0.0022    0.0012    0.0000    0.0000    0.0000    0      0
%             7    8     0.0042    0.0013    0.0000    0.0420    0.0210    0      0
%             7    9     0.0022    0.0012    0.0000    0.0850    0.0400    0      1
%             9    10    0.0038    0.0021    0.0000    0.0420    0.0210    0      0
%             10   11    0.0043    0.0025    0.0000    0.1400    0.0700    0      1
%             11   12    0.0027    0.0015    0.0000    0.1260    0.0620    0      0
%             12   13    0.0027    0.0015    0.0000    0.0000    0.0000    0      0
%             13   14    0.0026    0.0008    0.0000    0.0850    0.0400    0      0
%             13   15    0.0027    0.0015    0.0000    0.0420    0.0210    0      1
%             10   16    0.0068    0.0022    0.0000    0.0000    0.0000    0      0
%             16   17    0.0167    0.0054    0.0001    0.0420    0.0210    0      2
%             16   18    0.0026    0.0008    0.0000    0.0850    0.0400    0      0
%              3   19    0.0031    0.0010    0.0000    0.0420    0.0210    0      0
%             19   20    0.0019    0.0011    0.0000    0.0420    0.0210    0      0
%             20   21    0.0026    0.0008    0.0000    0.1260    0.0630    0      0
%             20   22    0.0037    0.0012    0.0000    0.0420    0.0210    0      1
%              2   23    0.0024    0.0014    0.0000    0.0850    0.0400    0      0
%             23   24    0.0035    0.0020    0.0000    0.0000    0.0000    0      0
%             24   25    0.0010    0.0003    0.0000    0.0380    0.0180    0      1
%             25   26    0.0068    0.0022    0.0000    0.0850    0.0400    0      2
%             24   27    0.0054    0.0031    0.0000    0.0850    0.0400    0      0
%             27   28    0.0040    0.0023    0.0000    0.0000    0.0000    0      0
%             28   29    0.0037    0.0012    0.0000    0.0420    0.0210    0      0
%             27   30    0.0120    0.0039    0.0000    0.0000    0.0000    0      0
%             30   31    0.0016    0.0005    0.0000    0.1610    0.0800    0      1
%             30   32    0.0099    0.0032    0.0000    0.0420    0.0210    0      2
%              2   33    0.0052    0.0017    0.0000    0.0000    0.0000    0      0
%             33   34    0.0031    0.0010    0.0000    0.0850    0.0400    0      0
%             33   35    0.0042    0.0013    0.0000    0.0930    0.0440    0      2];
% 
% Sistema = Feeder;
% 
% NL = length(Sistema(:,1));  % number of lines
% N = NL + 1;                % number of nodes
% Ybus  = zeros(N);          % initialize Ybus
% SP = zeros(N,1);           % vector to save constant power loads
% SZ = zeros(N,1);           % to save constant impedance loads
% SI = zeros(N,1);           % to save constat current loads
% Mi = zeros(NL,N);
% Z = zeros(NL,1);
% for k = 1:NL                % calculate the Ybus and separate loads
%      N1 = Sistema(k,1);     % node send
%      N2 = Sistema(k,2);     % node receive
%      ykm = 1/(Sistema(k,3)+j*Sistema(k,4));
%      bkm = j*Sistema(k,5);
%      Ybus(N1,N1) = Ybus(N1,N1) + ykm + bkm;
%      Ybus(N1,N2) = Ybus(N1,N2) - ykm;
%      Ybus(N2,N1) = Ybus(N2,N1) - ykm;
%      Ybus(N2,N2) = Ybus(N2,N2) + ykm + bkm;
%      alfa = Sistema(k,9);
%      S = (Sistema(k,6)+j*Sistema(k,7));
%      if alfa  == 0
%          SP(N2) = -S;
%      end
%      if alfa  == 1
%          SI(N2) = -S;
%      end
%      if alfa  == 2
%          SZ(N2) = -S;
%      end
%      % matrix de incidencia
%      Mi(k,N1)=1;
%      Mi(k,N2)=-1;
%      Z(k,1)= (Sistema(k,3)+j*Sistema(k,4));
% 
% end
N = 2;
%kN = 2:N;  % all nodes except the slack (slack = 1)
%YNS = Ybus(kN,1);   % (vector of the Ybus related to the slack)
%YNN = Ybus(kN,kN);  % (vector of the Ybus related to other nodes)

%G = real(Ybus);
%B = imag(Ybus);
G = [ 0.3846 -0.3846; -0.3846 0.3846];
B = [-1.9231 1.9231; 1.9231 -1.9231];

%Yup = [G -B];
%Ydown = [B G];
%Y = [G -B; B G];

Pd = [0.1400 0.1260];
Qd = [0.0700 0.0600];
C = [7 8 9 10];

Pgmax = [1 1 1 1]';
Pgmin = [0 0 0 0]';
Qgmax = [1 1 1 1]';
Qgmin = [0 0 0 0]';

% generadores
Gen = [1 1 0 0; 0 0 1 1];

cvx_begin 
    % generadores
    variable Pg(4,1);
    variable Qg(4,1);
    % potencia en los nodos
    variable P(N,1)
    variable Q(N,1);

    % relajacion SOCP
    variable W(2*N,2*N) symmetric;
    
    W(:,N+1) == 0;
    W(N+1,:) == 0;
    W(1,1)   == 1;

    minimize(C*Pg);

    subject to
        % Balance de potencia
        for k=1 : N
            % itera sobre nodos
            P(k) == W(k,1)*G(k,1) - W(k,N+1)*B(k,1) + W(N+k,N+1)*G(k,1) + W(N+k,1)*B(k,1);
            Q(k) == -W(k,N+1)*G(k,1) - W(k,1)*B(k,1) + W(N+k,1)*G(k,1) - W(N+k,N+1)*B(k,1);
            
            sum_p = 0;
            sum_q = 0;
            
            for m=1 : N
               sum_p == sum_p + G(k,m)*W(k,m) - W(k,N+m)*B(k,m) - W(N+k,N+m)*G(k,m) - W(N+k,m)*B(k,m);
               sum_q == sum_q - W(k,N+m)*G(k,m) + W(k,m)*B(k,m) + W(N+k,m)*G(k,m) + W(N+k,N+m)*B(k,m);
               if(k==1 && m==2)   
                % relajacion SOCP
                norm([2*W(k,m); W(k,k) - W(m,m)]) <= W(k,k) + W(m,m);
                norm([2*W(k,N+m); W(k,k) - W(N+m,N+m)]) <= W(k,k) + W(N+m,N+m);
                norm([2*W(N+k,m); W(N+k,N+k) - W(m,m)]) <= W(N+k,N+k) + W(m,m);
                norm([2*W(N+k,N+m); W(N+k,N+k) - W(N+m,N+m)]) <= W(N+k,N+k) + W(N+m,N+m);
               end
            end
            P(k) == P(k) + sum_p;
            Q(k) == Q(k) + sum_q;
            
        end 
        P == (Gen * Pg) - Pd';
        Q == (Gen * Qg) - Qd';
%         Pg(1) + Pg(2) == P(1) + Pd(1);
%         Pg(2) + Pg(3) == P(2) + Pd(2);
%         Qg(1) + Qg(2) == Q(1) + Qd(1);
%         Qg(2) + Qg(3) == Q(2) + Qd(2);
 
        % limites de potencias
        Pg <= Pgmax;
        Pg >= Pgmin;
        Qg <= Qgmax;
        Qg >= Qgmin;
        
        W(1,1) + W(3,3) <= 1.609;
        W(1,1) + W(3,3) >= 0.9409;
        W(2,2) + W(4,4) <= 1.609;
        W(2,2) + W(4,4) >= 0.9409;
      
        %diag(W) >= 0;
        % Limete de voltajes

     cvx_end
   W

% recuperacion de la variable original
% [v,l] = eig(W);
% landa = l(end);
% phi = v(:,end);
% X = (phi*sqrt(landa))*(sqrt(landa)*phi');
% V = (phi*sqrt(landa))*-1;
% cmp = abs(W - X);
