clear all

%       Node1 - Node2 - rkm   -   xkm -    bkm/2   -   P   -   Q   -   D -   alpha
<<<<<<< HEAD
%Feeder = [ 1    2     0.0025    0.0026    0.0001    0.0850    0.0400    0      0]
=======
%Feeder = [1    2     0.0025    0.0026    0.0001    4.40384836e-10      3.91042860e-10  0      0
%          2    3     0.0034    0.0036    0.0001    8.50000000e-01      4.00000000e-01  0      0];
>>>>>>> 5c3fb13abe8b55dbe480ba0d28491adaab962b22

 Feeder = [  1    2     0.0025    0.0026    0.0001    0.0000    0.0000    0      0
             2    3     0.0034    0.0036    0.0001    0.08500    0.04000    0      0];
%             4    5     0.0013    0.0008    0.0000    0.0000    0.0000    0      0
%             5    6     0.0040    0.0023    0.0000    0.0850    0.0400    0      2
%             5    7     0.0022    0.0012    0.0000    0.0000    0.0000    0      0
%             7    8     0.0042    0.0013    0.0000    0.0420    0.0210    0      0
%             7    9     0.0022    0.0012    0.0000    0.0850    0.0400    0      1
%             9    10    0.0038    0.0021    0.0000    0.0420    0.0210    0      0
%             10   11    0.0043    0.0025    0.0000    0.1400    0.0700    0      1
%             11   12    0.0027    0.0015    0.0000    0.1260    0.0620    0      0
%             12   13    0.0027    0.0015    0.0000    0.0000    0.0000    0      0
%             13   14    0.0026    0.0008    0.0000    0.0850    0.0400    0      0
%             13   15    0.0027    0.0015    0.0000    0.0420    0.0210    0      1
%             10   16    0.0068    0.0022    0.0000    0.0000    0.0000    0      0
%             16   17    0.0167    0.0054    0.0001    0.0420    0.0210    0      2
%             16   18    0.0026    0.0008    0.0000    0.0850    0.0400    0      0
%              3   19    0.0031    0.0010    0.0000    0.0420    0.0210    0      0
%             19   20    0.0019    0.0011    0.0000    0.0420    0.0210    0      0
%             20   21    0.0026    0.0008    0.0000    0.1260    0.0630    0      0
%             20   22    0.0037    0.0012    0.0000    0.0420    0.0210    0      1
%              2   23    0.0024    0.0014    0.0000    0.0850    0.0400    0      0
%             23   24    0.0035    0.0020    0.0000    0.0000    0.0000    0      0
%             24   25    0.0010    0.0003    0.0000    0.0380    0.0180    0      1
%             25   26    0.0068    0.0022    0.0000    0.0850    0.0400    0      2
%             24   27    0.0054    0.0031    0.0000    0.0850    0.0400    0      0
%             27   28    0.0040    0.0023    0.0000    0.0000    0.0000    0      0
%             28   29    0.0037    0.0012    0.0000    0.0420    0.0210    0      0
%             27   30    0.0120    0.0039    0.0000    0.0000    0.0000    0      0
%             30   31    0.0016    0.0005    0.0000    0.1610    0.0800    0      1
%             30   32    0.0099    0.0032    0.0000    0.0420    0.0210    0      2
%              2   33    0.0052    0.0017    0.0000    0.0000    0.0000    0      0
%             33   34    0.0031    0.0010    0.0000    0.0850    0.0400    0      0
%             33   35    0.0042    0.0013    0.0000    0.0930    0.0440    0      2];

Sistema = Feeder;

NL = length(Sistema(:,1));  % number of lines
N = NL + 1;                % number of nodes
Ybus  = zeros(N);          % initialize Ybus
SP = zeros(N,1);           % vector to save constant power loads
SZ = zeros(N,1);           % to save constant impedance loads
SI = zeros(N,1);           % to save constat current loads
Mi = zeros(NL,N);
Z = zeros(NL,1);
for k = 1:NL                % calculate the Ybus and separate loads
     N1 = Sistema(k,1);     % node send
     N2 = Sistema(k,2);     % node receive
     ykm = 1/(Sistema(k,3)+j*Sistema(k,4));
     bkm = j*Sistema(k,5);
     Ybus(N1,N1) = Ybus(N1,N1) + ykm + bkm;
     Ybus(N1,N2) = Ybus(N1,N2) - ykm;
     Ybus(N2,N1) = Ybus(N2,N1) - ykm;
     Ybus(N2,N2) = Ybus(N2,N2) + ykm + bkm;
     alfa = Sistema(k,9);
     S = (Sistema(k,6)+j*Sistema(k,7));
     if alfa  == 0
         SP(N2) = -S;
     end
     if alfa  == 1
         SI(N2) = -S;
     end
     if alfa  == 2
         SZ(N2) = -S;
     end
     % matrix de incidencia
     Mi(k,N1)=1;
     Mi(k,N2)=-1;
     Z(k,1)= (Sistema(k,3)+j*Sistema(k,4));

end

kN = 2:N;  % all nodes except the slack (slack = 1)
YNS = Ybus(kN,1);   % (vector of the Ybus related to the slack)
YNN = Ybus(kN,kN);  % (vector of the Ybus related to other nodes)

G = real(Ybus);
B = imag(Ybus);
Yup = [G -B];
Ydown = [B G];
Y = [G -B; B G];

Pd = real(SP + SI + SZ)
Qd = imag(SP + SI + SZ)


Pgmax = [2 2 0 ]';
Pgmin = [0 0 0 ]';
Qgmax = [2 2 0 ]';
Qgmin = [0 0 0 ]';
% Pgmax = [2   0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]';
% Pgmin = [0   0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]';
% Qgmax = [2   0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]';
% Qgmin = [0   0  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]';


cvx_begin sdp

    variable W(2*N,2*N) symmetric;
    variable P(N,1);
    variable Q(N,1);

    W(:,N+1) == 0;
    W(N+1,:) == 0;
    W(1,1)   == 1;


    C = W(1:N, 1:N);
    D = W(1:N, N+1:2*N );
    E = W(N+1:2*N, 1:N);
    F = W(N+1:2*N, N+1:2*N);

    Lr = diag(G*C);
    Or = diag(G*F);
    Pr = diag(B*D);
    Qr = diag(-B*E);
    Li = diag(G*D);
    Oi = diag(-G*E);
    Pi = diag(-B*C);
    Qi = diag(-B*F);

    %para limite termico
    AA = Mi*D*Mi';
    BB = Mi*F*Mi';

    minimize(trace(Y*W));

    subject to
        % Balance de potencia
        Lr + Or + Pr + Qr == P;
        Li + Oi + Pi + Qi == Q;

        % limites de potencias
        P - Pd <= Pgmax;
        Q - Qd <= Qgmax;
        P - Pd >= Pgmin;
        Q - Qd >= Qgmin;

        % Limete de voltajes
        diag(C) + diag(F) <= 1.0609 * ones(N,1);
        diag(C) + diag(F) >= 0.9409 * ones(N,1);

        %limite termico

        % diag(AA)+ diag(BB) <= (100*abs(Z)).^2;

        W >= 0;

     cvx_end
   W;

% recuperacion de la variable original
[v,l] = eig(W);
landa = l(end);
phi = v(:,end);
X = (phi*sqrt(landa))*(sqrt(landa)*phi');
V = (phi*sqrt(landa))*-1
cmp = abs(W - X);
