import numpy as np
import pandas as pd
import cvxpy as cvx
import util
'''
queda pendiente agregar los paneles solares se esta trabajando con el sistema de
35 nodos y en la carpeta socp_relaxation_opf, desde util get data se propone leer
la info de la sheetname pv y devolverla ya aca en este modulo se define el Ppv_max
para pasarle al modelo

'''


def getA(tw):
    '''
    Esta matriz se usa para la operacion matricial de Pt= E(t) - E(t-1)
    '''
    A = np.zeros((tw, tw + 1))
    for i in range(tw):
        A[i, i] = 1
        A[i, i + 1] = -1
    return A


def print_variables(lines, C, S):
    for i, j in lines:
        i = i - 1
        j = j - 1
        print('line (i,j): ', i + 1, j + 1)
        print('Cii:', C[i, i], 'Cij:', C[i, j], ' Sij', S[i, j])


def solve(Cost,
          Pvpp_da,
          Est_init,
          Est_end,
          Pst_max,
          Sst_max,
          Pgmax_slack,
          Pgmin_slack,
          Qgmax_slack,
          Qgmin_slack,
          Pdiesel_max,
          Sdiesel_max,
          diesel_a_param,
          diesel_b_param,
          lt_diesel_cost,
          radiation,
          Ppv_max_fixed,
          Spv_max,
          pv_nodes,
          pv_area,
          G,
          B,
          N,
          lines,
          Pd,
          Qd,
          tw=4,
          rho=100):

    # para correr este modelo en una ventana de tiempo tw=4
    # cost: vector of tw elements

    tw_1s = np.ones((tw, 1))
    N_1s = np.ones(N)

    # matriz for P(t) = E(t) - E(t-1)
    A = getA(tw)

    # organizamos la informacion de la Ppv
    # max Ppv predicted
    Ppv_pred = [(pv_area * i) / 1e6 for i in radiation]
    tmp = [[min(pv_pred[i], pv_max) for i, pv_max in enumerate(Ppv_max_fixed)]
           for pv_pred in Ppv_pred]
    Ppv_max = []
    Spv_max_model = []
    for i in tmp:
        pv_i = np.zeros(N)
        pv_i[pv_nodes - 1] = i
        spv_max_i = np.zeros(N)
        spv_max_i[pv_nodes - 1] = Spv_max
        Ppv_max.append(pv_i)
        Spv_max_model.append(spv_max_i)

    Ppv_max = np.array(Ppv_max)
    Spv_max_model = np.array(Spv_max_model)

    Pgmax_slack = tw_1s @ np.reshape(Pgmax_slack, (1, N))
    Pgmin_slack = tw_1s @ np.reshape(Pgmin_slack, (1, N))
    Qgmax_slack = tw_1s @ np.reshape(Qgmax_slack, (1, N))
    Qgmin_slack = tw_1s @ np.reshape(Qgmin_slack, (1, N))
    Pdiesel_max = tw_1s @ np.reshape(Pdiesel_max, (1, N))
    Sdiesel_max = tw_1s @ np.reshape(Sdiesel_max, (1, N))
    Pst_max = tw_1s @ np.reshape(Pst_max, (1, N))
    Sst_max = tw_1s @ np.reshape(Sst_max, (1, N))

    # variables
    P = cvx.Variable(tw, N)
    Q = cvx.Variable(tw, N)

    Pslack = cvx.Variable(tw, N)
    Qslack = cvx.Variable(tw, N)
    Pdiesel = cvx.Variable(tw, N)
    Qdiesel = cvx.Variable(tw, N)
    Ppv = cvx.Variable(tw, N)
    Qpv = cvx.Variable(tw, N)
    Est = cvx.Variable(tw + 1, N)
    Pst = cvx.Variable(tw, N)
    Qst = cvx.Variable(tw, N)

    Pvpp = cvx.Variable(tw)

    # variables for SOC relaxation
    C = {}
    S = {}
    for i in range(tw):
        C[i] = cvx.Symmetric(N)
        S[i] = cvx.Variable(N, N)

    # Costo de generacion slack
    slack_cost = cvx.sum_entries(Cost * Pslack)
    cost_pg = cvx.sum_entries(Cost * P)

    # print('Pgmin_slack:', Pgmin_slack)
    # costo de generacion diesel
    ax2 = Pdiesel**2 * np.diag(diesel_a_param)
    bx = Pdiesel * np.diag(diesel_b_param)
    diesel_cost = cvx.sum_entries((ax2 + bx) * np.diag(lt_diesel_cost))

    # costo de generacion paneles solares
    pv_cost = cvx.sum_entries((0.8 * Cost) * Ppv)

    # Costo de generacion de la VPP
    penalty = 1e6 * (cvx.norm1(Pvpp - Pvpp_da))
    vpp_cost = diesel_cost + pv_cost + penalty

    # Fobj
    # el problema con esta fx es que no despacha Ppv
    # fx1 = slack_cost + vpp_cost
    # no despacha Ppv
    fx2 = penalty + diesel_cost + cost_pg + slack_cost
    # fx = Cost * Pvpp
    obj = cvx.Minimize(fx2)

    global_constraints = [
        Pvpp == Pdiesel * N_1s + Ppv * N_1s + Pst * N_1s,
        P == Pslack + Pdiesel + Ppv + Pst,
        Q == Qslack + Qdiesel + Qpv + Qst,

        # slack
        Pslack <= Pgmax_slack,
        Pslack >= Pgmin_slack,
        Qslack <= Qgmax_slack,
        Qslack >= Qgmin_slack,

        # diesel
        Pdiesel <= Pdiesel_max,
        Pdiesel >= 0,
        Pdiesel**2 + Qdiesel**2 <= Sdiesel_max**2,

        # paneles
        Ppv <= Ppv_max,
        Ppv >= np.zeros((tw, N)),
        Ppv**2 + Qpv**2 <= Spv_max_model**2,

        # Batteries
        Est[0, :].T == Est_init,
        Est[tw, :].T == Est_end,
        Pst == A * Est,
        Est >= 0,
        cvx.abs(Pst) <= Pst_max,
        Pst**2 + Qst**2 <= Sst_max**2
    ]

    for t in range(tw):
        # constraints
        constraints = [
            # V1 = 1
            C[t][0, 0] == 1,
            # Sii does not exit
            cvx.diag(S[t]) == np.zeros(N),

            # antisimetric matrix
            S[t] == -S[t].T,

            # power balance
            P[t, :] == (cvx.diag(G * C[t]) - cvx.diag(B * S[t].T) + Pd[t]).T,
            Q[t, :] == (-cvx.diag(B * C[t]) - cvx.diag(G * S[t].T) + Qd[t]).T,

            # vmin^2 <= cii <= vmax^2
            cvx.diag(C[t]) <= np.ones(N) * Vmax**2,
            cvx.diag(C[t]) >= np.ones(N) * Vmin**2,
        ]

        # SOCP restrictions
        for i, j in lines:
            i = i - 1
            j = j - 1
            lhs_i = cvx.vstack(C[t][i, j], S[t][i, j],
                               ((C[t][i, i] - C[t][j, j]) / 2))
            rhs_i = (C[t][i, i] + C[t][j, j]) / 2

            lhs_j = cvx.vstack(C[t][j, i], S[t][j, i],
                               ((C[t][j, j] - C[t][i, i]) / 2))
            rhs_j = (C[t][j, j] + C[t][i, i]) / 2

            constraints.append(cvx.norm(lhs_i) <= rhs_i)
            constraints.append(cvx.norm(lhs_j) <= rhs_j)

        global_constraints += constraints

    problem = cvx.Problem(obj, global_constraints)
    problem.solve()
    print('Cost:\n', Cost)
    print('Status:', problem.status)
    # print('Fobj:', problem.value)
    print('\nPg:\n', cvx.sum_entries(P, axis=1).value)
    print('\nPd:\n', np.sum(Pd, axis=1))
    print('\nPdiesel:\n', cvx.sum_entries(Pdiesel, axis=1).value)
    # print('Ppv:', Ppv.value)
    print('\nPpv:\n', cvx.sum_entries(Ppv, axis=1).value)
    print('\nPpv_max:\n', cvx.sum_entries(Ppv_max, axis=1).value)
    print('\nPst:\n', cvx.sum_entries(Pst, axis=1).value)
    # print('\nPst_max:\n', cvx.sum_entries(Pst_max, axis=1).value)
    print('\nPslack:\n', cvx.sum_entries(Pslack, axis=1).value)
    print('Pvpp_da:     ', Pvpp_da)
    print('Pvpp:\n', Pvpp.value)
    print('slack_cost:  ', slack_cost.value)
    print('Diesel_cost: ', diesel_cost.value)
    print('Ppv_cost:    ', pv_cost.value)
    print('Penalty:     ', penalty.value)
    print('Vpp_cost:    ', vpp_cost.value)
    print('Vpp_profit:  ', (Cost * Pvpp).value - diesel_cost.value)
    print('dayahead err:', cvx.abs(Pvpp_da - Pvpp).value)
    exit()
    # theta = util.get_thetas(lines, C.value, S.value)
    # V = util.get_voltages(theta, C.value)
    # Pg = np.array(P.value.ravel().tolist()[0])
    # Qg = np.array(Q.value.ravel().tolist()[0])
    # return V, Pg - Pd, Qg - Qd, S.value, C.value


# print(V.shape)

if __name__ == "__main__":
    testcase = 'input/in35.xlsx'
    data = util.getData(testcase)
    N = len(data['lines']) + 1

    # time window in hours
    tw = 4
    # penalization
    rho = 100

    #  load external cost data
    cost_df = pd.read_excel(testcase, sheetname='radiacion')
    # load external Est_init and Est_end data
    st_df = pd.read_excel(testcase, sheetname='batteries')

    #Pvpp_df = pd.read_excel(testcase, sheetname='Pvpp')

    # spot energy market prices
    Cost = cost_df['Cost_med'].values
    radiation = cost_df['Radiacion_med'].values
    print('radiation:', radiation)
    # potencia maxima dada la radiacion predicha
    #Ppv_max_predicted = cost_df['Radiacion_med'].values * (100 / 1000000)

    # Pvpp from day-ahead
    #Pvpp_da = Pvpp_df['Pvpp'][8:12].values / 1000

    # Batteries
    st_nodes = data['st_nodes']
    Pst_max_i = data['Pst_max_i']
    Pst_max = np.zeros(N)
    Pst_max[st_nodes - 1] = Pst_max_i / 1000

    Est_max_i = data['Est_max_i']
    Est_max = np.zeros(N)
    Est_max[st_nodes - 1] = Est_max_i / 1000

    Sst_max_i = data['Sst_max_i']
    Sst_max = np.zeros(N)
    Sst_max[st_nodes - 1] = Sst_max_i / 1000

    # Est_init
    Est_init_i = st_df['Est_init'].values
    Est_init = np.zeros(N)
    Est_init[st_nodes - 1] = Est_init_i / 1000

    # Est_end
    Est_end_i = st_df['Est_end'].values
    Est_end = np.zeros(N)
    Est_end[st_nodes - 1] = Est_end_i / 1000

    # demand in time window
    Pd = [data['Pd'] + (i / 1000) for i in range(tw)]
    Pd = np.array(Pd)

    Qd = [data['Qd'] + (i / 1000) for i in range(tw)]
    Qd = np.array(Qd)

    # parameters
    # Pst_max = data['Pst_max']
    # Sst_max = data['Sst_max']
    Ppv_max_fixed = data['Ppv_max']
    Spv_max = data['Spv_max']
    pv_nodes = data['pv_nodes']
    pv_area = data['pv_area']
    tmp = np.zeros(N)

    Pdiesel_max = data['Pdiesel_max']
    Sdiesel_max = data['Sdiesel_max']
    diesel_a_param = data['diesel_a_param']
    diesel_b_param = data['diesel_b_param']
    lt_diesel_cost = data['lt_diesel_cost']
    G = data['G']
    B = data['B']
    lines = data['lines']
    Pgmax_slack = data['Pg_max_slack']
    Pgmin_slack = data['Pg_min_slack']
    Qgmax_slack = data['Qg_max_slack']
    Qgmin_slack = data['Qg_min_slack']
    Vmin = data['Vmin']
    Vmax = data['Vmax']
    Pvpp_da = data['Pvpp_da'][0:tw]
    # print('Est_init:\n', Est_init)
    # print('Est_end:\n', Est_end)
    # print('Pst_max:\n', Pst_max)
    # print('Sst_max:\n', Sst_max)
    # print('Ppv_max:\n', Ppv_max)
    # print('Spv_max:\n', Spv_max)
    print('radiation:\n', radiation)
    print('Ppv_max_fixed:', Ppv_max_fixed)
    print('pv_nodes:', pv_nodes)
    print('pv_area:', pv_area)
    # print('Pdiesel_max:\n', Pdiesel_max)
    # print('Sdiesel_max:\n', Sdiesel_max)
    # print('Pd:\n', np.sum(Pd[0]))
    # print('Pg_max_slack:\n', Pg_max_slack)
    # print('Pg_min_slack:\n', Pg_min_slack)
    # print('Qg_max_slack:\n', Qg_max_slack)
    # print('Qg_min_slack:\n', Qg_min_slack)
    # print('Pd:\n', Pd[0], 'len:', len(Pd[0]))
    # print('Qd:\n', Qd[0], 'len:', len(Qd[0]))
    # solve(Cost, Pgmax, Pgmin, Qgmax, Qgmin, G, B, N, lines, Pd, Qd):
    # solve(Cost, Pgmax, Pgmin, Qgmax, Qgmin, G, B, N, lines, Pd, Qd):
    # print('Pvpp_da:', Pvpp_da)
    exit()
    solve(Cost, Pvpp_da, Est_init, Est_end, Pst_max, Sst_max, Pgmax_slack,
          Pgmin_slack, Qgmax_slack, Qgmin_slack, Pdiesel_max, Sdiesel_max,
          diesel_a_param, diesel_b_param, lt_diesel_cost, radiation,
          Ppv_max_fixed, Spv_max, pv_nodes, pv_area, G, B, N, lines, Pd, Qd)
    exit()
