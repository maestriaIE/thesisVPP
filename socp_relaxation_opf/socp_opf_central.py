import numpy as np
import pandas as pd
import cvxpy as cvx
import util

testcase = 'input/in35.csv'
params = testcase[:-4] + '_parameters.csv'

# load dataframes with feeder data
df = pd.read_csv(testcase)
df_param = pd.read_csv(params)

# get lines of power system
lines = list(zip(df['Node1'], df['Node2']))

#
Ybus, SP, SI, SZ = util.get_Ybus(df)
G = np.real(Ybus)  # matriz conductancia
B = np.imag(Ybus)  # matriz susceptancia
Pd = -np.real(SP + SI + SZ)
Qd = -np.imag(SP + SI + SZ)

Cost = df_param['Cost'].values
Pgmax = df_param['Pgmax'].values
Pgmin = df_param['Pgmin'].values
Qgmax = df_param['Pgmax'].values
Qgmin = df_param['Qgmin'].values

Vmin = 0.8
Vmax = 1.1

N = len(lines) + 1

SDP_sol = np.array([
    0.999999999994254, 0.996808118359748, 0.992440505766281, 0,
    -0.00121059970728328, -0.00291107487426425
])


def print_variables(lines, C, S):
    for i, j in lines:
        i = i - 1
        j = j - 1
        print('line (i,j): ', i + 1, j + 1)
        print('Cii:', C[i, i], 'Cij:', C[i, j], ' Sij', S[i, j])


def matricial(Cost, Pgmax, Pgmin, Qgmax, Qgmin, G, B, N, lines, Pd, Qd):

    # variables
    P = cvx.Variable(N)
    Q = cvx.Variable(N)
    C = cvx.Symmetric(N)
    #C = cvx.Variable(N, N)
    S = cvx.Variable(N, N)

    # Fobj
    fx = Cost * P

    # constraints
    constraints = [
        # Sii does not exit
        C[0, 0] == 1,
        cvx.diag(S) == np.zeros(N),

        # power balance
        P == cvx.diag(G * C) - cvx.diag(B * S.T) + Pd,
        Q == -cvx.diag(B * C) - cvx.diag(G * S.T) + Qd,

        # vmin^2 <= cii <= vmax^2
        cvx.diag(C) <= np.ones(N) * Vmax**2,
        cvx.diag(C) >= np.ones(N) * Vmin**2,

        # Power limits
        P >= Pgmin,
        P <= Pgmax,
        Q >= Qgmin,
        Q <= Qgmax,lkñk

        # antisimetric matrix
        S == -S.T
    ]

    for i in range(0, N):
        for j in range(0, N):
            if G[i, j] != 0:
                lhs = cvx.vstack(C[i, j], S[i, j], ((C[i, i] - C[j, j]) / 2))
                rhs = (C[i, i] + C[j, j]) / 2
                constraints.append(cvx.norm(lhs) <= rhs)

    # revisar la matix C si se define simetrica
    # for i, j in lines:
    #     i = i - 1
    #     j = j - 1
    #     constraints.append(C[i, j] == C[j, i])
    #     constraints.append(S[i, j] == -S[j, i])

    #     # i -> j
    #     lhs = cvx.vstack(C[i, j], S[i, j], ((C[i, i] - C[j, j]) / 2))
    #     rhs = (C[i, i] + C[j, j]) / 2
    #     constraints.append(cvx.norm(lhs) <= rhs)

    #     # j -> i
    #     lhs = cvx.vstack(C[j, i], S[j, i], ((C[j, j] - C[i, i]) / 2))
    #     rhs = (C[j, j] + C[i, i]) / 2
    #     constraints.append(cvx.norm(lhs) <= rhs)

    obj = cvx.Minimize(fx)
    problem = cvx.Problem(obj, constraints)
    print(problem.solve(verbose=True))
    print('Pg:', cvx.sum_entries(P).value)
    print('Pd:', np.sum(Pd))
    theta = util.get_thetas(lines, C.value, S.value)
    V = util.get_voltages(theta, C.value)
    Pg = np.array(P.value.ravel().tolist()[0])
    Qg = np.array(Q.value.ravel().tolist()[0])
    return V, Pg - Pd, Qg - Qd, S.value, C.value


print('Cost:\n', Cost)
print('Pg_max:\n', Pgmax)
print('Pg_min:\n', Pgmin)
print('Qg_max:\n', Qgmax)
print('Qg_min:\n', Qgmin)
print('Pd:\n', Pd, 'len:', len(Pd))
print('Qd:\n', Qd, 'len:', len(Qd))

V, P, Q, S, C = matricial(Cost, Pgmax, Pgmin, Qgmax, Qgmin, G, B, N, lines, Pd,
                          Qd)
# print(V.shape)

opt_result_35 = np.array([
    1.0000 + 0.0000j, 0.9973 - 0.0010j, 0.9940 - 0.0022j, 0.9896 - 0.0025j,
    0.9883 - 0.0027j, 0.9879 - 0.0027j, 0.9863 - 0.0028j, 0.9861 - 0.0027j,
    0.9845 - 0.0028j, 0.9818 - 0.0030j, 0.9796 - 0.0031j, 0.9787 - 0.0032j,
    0.9783 - 0.0032j, 0.9780 - 0.0032j, 0.9781 - 0.0032j, 0.9808 - 0.0028j,
    0.9800 - 0.0027j, 0.9805 - 0.0028j, 0.9931 - 0.0021j, 0.9926 - 0.0021j,
    0.9922 - 0.0021j, 0.9924 - 0.0021j, 0.9957 - 0.0011j, 0.9936 - 0.0013j,
    0.9935 - 0.0012j, 0.9928 - 0.0012j, 0.9913 - 0.0014j, 0.9911 - 0.0014j,
    0.9909 - 0.0014j, 0.9885 - 0.0010j, 0.9882 - 0.0009j, 0.9880 - 0.0009j,
    0.9962 - 0.0009j, 0.9959 - 0.0008j, 0.9958 - 0.0008j
])

opt_result = np.array([
    1.0000 + 0.0000j, 0.9959 + 0.0013j, 0.9959 + 0.0013j, 0.9919 + 0.0027j,
    0.9917 + 0.0026j, 0.9916 + 0.0024j, 0.9916 + 0.0024j, 0.9915 + 0.0024j,
    0.9915 + 0.0024j, 0.9915 + 0.0009j, 0.9889 + 0.0009j, 0.9878 + 0.0004j,
    0.9871 + 0.0003j, 0.9866 + 0.0004j, 0.9865 + 0.0005j, 0.9863 + 0.0005j,
    0.9862 + 0.0004j, 0.9840 - 0.0008j, 0.9811 - 0.0020j, 0.9780 - 0.0029j,
    0.9764 - 0.0035j, 0.9757 - 0.0035j, 0.9688 - 0.0026j, 0.9677 - 0.0024j,
    0.9674 - 0.0023j, 0.9673 - 0.0023j, 0.9672 - 0.0023j, 0.9893 + 0.0016j,
    0.9873 + 0.0005j, 0.9735 - 0.0061j, 0.9608 - 0.0078j, 0.9564 - 0.0063j,
    0.9480 - 0.0078j, 0.9401 - 0.0092j, 0.9344 - 0.0094j, 0.9705 - 0.0069j,
    0.9697 - 0.0069j, 0.9808 + 0.0009j, 0.9750 + 0.0012j, 0.9717 - 0.0001j,
    0.9682 + 0.0002j, 0.9577 - 0.0012j, 0.9566 - 0.0011j, 0.9562 - 0.0010j,
    0.9559 - 0.0009j, 0.9558 - 0.0009j, 0.9284 - 0.0083j, 0.9251 - 0.0076j,
    0.9211 - 0.0067j, 0.9174 - 0.0059j, 0.9150 - 0.0058j, 0.9141 - 0.0058j,
    0.9120 - 0.0052j, 0.9100 - 0.0047j, 0.9836 + 0.0018j, 0.9800 + 0.0032j,
    0.9769 + 0.0044j, 0.9708 + 0.0047j, 0.9698 + 0.0050j, 0.9689 + 0.0053j,
    0.9685 + 0.0055j, 0.9680 + 0.0051j, 0.9963 - 0.0014j, 0.9808 - 0.0089j,
    0.9691 - 0.0067j, 0.9622 - 0.0052j, 0.9511 - 0.0026j, 0.9344 - 0.0028j,
    0.9231 - 0.0002j, 0.8907 - 0.0033j, 0.8871 - 0.0026j, 0.8824 - 0.0014j,
    0.8781 - 0.0003j, 0.8748 - 0.0003j, 0.8741 - 0.0000j, 0.8724 + 0.0004j,
    0.8723 + 0.0005j, 0.9652 - 0.0077j, 0.9606 - 0.0074j, 0.9576 - 0.0071j,
    0.9554 - 0.0067j, 0.9544 - 0.0068j, 0.9538 - 0.0067j, 0.9536 - 0.0066j,
    0.9535 - 0.0066j, 0.9595 - 0.0077j, 0.9593 - 0.0078j, 0.9592 - 0.0079j,
    0.9581 - 0.0041j, 0.9537 - 0.0034j, 0.9494 - 0.0028j, 0.9485 - 0.0025j,
    0.9483 - 0.0024j, 0.9483 - 0.0024j, 0.9481 - 0.0022j, 0.9465 - 0.0034j,
    0.9449 - 0.0036j, 0.9447 - 0.0036j, 0.9446 - 0.0037j, 0.9963 + 0.0010j,
    0.9832 - 0.0036j, 0.9760 - 0.0030j, 0.9660 - 0.0013j, 0.9500 + 0.0043j,
    0.9445 + 0.0063j, 0.9340 + 0.0106j, 0.9291 + 0.0123j, 0.9191 + 0.0158j,
    0.9153 + 0.0172j, 0.9085 + 0.0199j, 0.9047 + 0.0213j, 0.9039 + 0.0216j,
    0.9037 + 0.0216j, 0.9929 + 0.0020j, 0.9923 + 0.0019j, 0.9912 + 0.0019j,
    0.9908 + 0.0018j, 0.9906 + 0.0017j
])

exit()
print('Voltages:', V)
print('P:', P)
print('Q:', Q)
print('err_voltages:\n', abs(opt_result_35 - V))
#print('max err', max(abs(opt_result - V)))
# prjnt_varjables(ljnes, C, S)

print('Pd:', np.sum(Pd))
print('Pgen:', np.sum(P))
