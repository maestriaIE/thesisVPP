#!/usr/bin/python
import numpy as np
import pandas as pd
from math import atan2, sqrt, sin, cos
from numpy.linalg import inv


def get_thetas(lines, C, S):
    '''
    given new variables Cii, Cij, Sij it return the angles in each nodes
    in the following sense:
    \theta_j - \theta_i = atan2(S_{i,j},C_{i,j})

    Parameters
    ----------
    lines: list of tuples describing lines [(i,j)]
    C    : 2D array with variables related to Cij (float)
    S    : 2D array with variables related to Sij (float)

    Returns
    ---------
    theta: numpy 1D array with angle of each node
    '''

    N = len(lines) + 1
    A = np.zeros((N, N))
    B = np.zeros(N)

    A[0, 0] = 1
    B[0] = 0

    for row, (i, j) in enumerate(lines):
        A[row + 1, i - 1] = -1
        A[row + 1, j - 1] = 1
        B[row + 1] = atan2(S[i - 1, j - 1], C[i - 1, j - 1])

    theta = np.linalg.solve(A, B)
    return theta


def get_voltages(theta, C):
    '''
    Get Voltages in complex form indicating Voltage magnitude and angle

    Parameters
    ----------
    theta : numpy 1D array with nodes theta
    C     : 2D array related to variables Cij (float)

    Returns
    ---------
    V : complex numpy 1D array with nodal voltage
    '''

    N = len(theta)
    V = np.zeros(N, dtype=complex)
    for i in range(0, N):
        v = sqrt(C[i, i])
        V[i] = complex(v * cos(theta[i]), v * sin(theta[i]))
    return V


def get_Ybus(data):
    '''
    Compute Ybus matriz from system data

    Parameters
    ----------
    Data : pandas dataFrame with info

    Returns
    ----------
    Ybus: numpy 2D complex array,
    SP,SI,SZ: numpy 1D complex array
    '''

    NL = len(data)
    NN = NL + 1

    Ybus = np.complex_(np.zeros([NN, NN]))  # initialize YBUS
    SP = np.complex_(np.zeros([NN]))  # to save constant power loads
    SZ = np.complex_(np.zeros([NN]))  # to save constant impedance loads
    SI = np.complex_(np.zeros([NN]))  # to save constant current loads

    for index, row in data.iterrows():
        N1 = int(row['Node1']) - 1
        N2 = int(row['Node2']) - 1
        ykm = 1 / complex(row['rkm'], row['xkm'])
        bkm = complex(0, row['bkm/2'])
        Ybus[N1, N1] += ykm + bkm
        Ybus[N1, N2] -= ykm
        Ybus[N2, N1] -= ykm
        Ybus[N2, N2] += ykm + bkm
        alfa = row['alpha']
        S = complex(row['Pd'], row['Qd'])
        if alfa == 0:
            SP[N2] = -S
        elif alfa == 1:
            SI[N2] = -S
        elif alfa == 2:
            SZ[N2] = -S

    return Ybus, SP, SI, SZ


def loadFlow(Ybus, SP, SI, SZ):
    '''
    Compute the load flow of system through Quadractic aproximation made by
    Alejandro Garces

    Parameters
    ----------
    Ybus : numpy 2D complex array
    SP,SI,SZ:  numpy 1D complex array

    Returns
    ---------
    Vs : numpy complex 1D array
    MM : ?
    A: ?
    '''
    NN = len(Ybus)
    NL = NN - 1
    YNS = Ybus[0, 1:]  # (vector of the Ybus related to the slack)
    YNN = Ybus[1:, 1:]  # (vector of the Ybus related to other nodes)

    A = YNS - 2 * np.conj(SP[1:]) - np.conj(SI[1:])
    B = np.diag(np.conj(SP[1:]))
    C = YNN - np.diag(np.conj(SZ[1:]))

    # print('A',len(A),'B',len(B),'x',len(B[0]),'C',len(C))
    MM = np.zeros((2 * (NN - 1), 2 * (NN - 1)))
    MM[:NL, :NL] = np.real(B + C)
    MM[:NL, NL:] = np.imag(B - C)
    MM[NL:, :NL] = np.imag(B + C)
    MM[NL:, NL:] = np.real(C - B)

    tmp = np.zeros(2 * NL)
    tmp[:NL] = -np.real(A)
    tmp[NL:] = -np.imag(A)
    VRI = inv(MM).dot(tmp)
    Vs = np.complex_(np.zeros(NN))
    Vs[0] = 1
    # print('VRI:',len(VRI))
    Vs[1:] = np.complex_(list(map(complex, VRI[:NL], VRI[NL:])))
    return Vs, MM, A


def getData(testcase):

    # load dataframes with feeder data
    system_df = pd.read_excel(testcase, sheetname='system')
    param_df = pd.read_excel(testcase, sheetname='parameters')
    pvpp_df = pd.read_excel(testcase, sheetname='Pvpp')
    pv_df = pd.read_excel(testcase, sheetname='pv')
    st_df = pd.read_excel(testcase, sheetname='batteries')
    # get lines of power system
    lines = list(zip(system_df['Node1'], system_df['Node2']))
    N = len(lines) + 1
    #
    Ybus, SP, SI, SZ = get_Ybus(system_df)
    G = np.real(Ybus)  # matriz conductancia
    B = np.imag(Ybus)  # matriz susceptancia
    Vmin = 0.9
    Vmax = 1.1

    # load Diesel data
    diesel_df = pd.read_excel(testcase, sheetname='diesel')
    Pdiesel_max = np.zeros(N)
    Pdiesel_max[diesel_df['Node'] - 1] = diesel_df['Pdiesel_max'].values
    Sdiesel_max = np.zeros(N)
    Sdiesel_max[diesel_df['Node'] - 1] = diesel_df['Sdiesel_max'].values
    diesel_a_param = np.zeros(N)
    diesel_a_param[diesel_df['Node'] - 1] = diesel_df['a'].values
    diesel_b_param = np.zeros(N)
    diesel_b_param[diesel_df['Node'] - 1] = diesel_df['b'].values
    lt_diesel_cost = np.zeros(N)
    lt_diesel_cost[diesel_df['Node'] - 1] = diesel_df['lt_diesel_cost'].values

    data = {
        'lines': lines,
        'Ybus': Ybus,
        'G': G,
        'B': B,
        'Pd': param_df['Pd'].values,
        'Qd': param_df['Qd'].values,
        'Pst_max_i': st_df['Pst_max'].values,
        'Est_max_i': st_df['Est_max'].values,
        'Sst_max_i': st_df['Sst_max'].values,
        'st_nodes': st_df['Node'].values,
        'Ppv_max': pv_df['Ppv_max'].values,
        'Spv_max': pv_df['Spv_max'].values,
        'pv_nodes': pv_df['Node'].values,
        'pv_area': pv_df['AreaPanel_m2'].values,
        'Pdiesel_max': Pdiesel_max / 1000,
        'Sdiesel_max': Sdiesel_max / 1000,
        'diesel_a_param': diesel_a_param,
        'diesel_b_param': diesel_b_param,
        'lt_diesel_cost': lt_diesel_cost,
        'Pg_max_slack': param_df['Pg_max_slack'].values,
        'Pg_min_slack': param_df['Pg_min_slack'].values,
        'Qg_max_slack': param_df['Qg_max_slack'].values,
        'Qg_min_slack': param_df['Qg_min_slack'].values,
        'Pvpp_da': pvpp_df['Pvpp'].values / 1000,
        'Vmin': Vmin,
        'Vmax': Vmax
    }

    return data


if __name__ == "__main__":
    # get input data
    testcase = 'input/in35.csv'
    df = pd.read_csv(testcase)

    # get Ybus
    Ybus, SP, SI, SZ = get_Ybus(df)

    # get voltage solutions (given nodal power)
    Vs, MM, A = loadFlow(Ybus, SP, SI, SZ)
