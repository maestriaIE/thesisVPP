* Distributed Formulation
*** Math model
   the problem to optimize is the cost of power production
   \[ \min_{P_g} \sum_{i=1}^N C(P_i^g) \]
   s.t
   \[ P_i^g - P_i^d = G_{ii}(e_i^2+f_i^2) + \sum_{j \in \delta(i)}[
   G_{ij}(e_i e_j + f_i f_j) -B_{ij}(e_i f_j - e_j f_i)] \]

    \[ Q_i^g - Q_i^d = -B_{ii}(e_i^2+f_i^2) + \sum_{j \in \delta(i)}[
   -B_{ij}(e_i e_j + f_i f_j) -G_{ij}(e_i f_j - e_j f_i)] \]

   \[ V_{min}^2 \leq e_i^2 + f_i^2 \leq V_{max}^2\]

   \[ P_i^{min} \leq P_i^g \leq P_i^{max} \]

   \[ Q_i^{min} \leq Q_i^g \leq Q_i^{max} \]

   we can observe that the nonlinearity and nonconvexity comes from one of the
   following three forms:

   1. \[ e_i^2 +f_i^2 = |V_i|^2\]
   2. \[ e_i e_j + f_i f_j = |V_i||V_j|cos(\theta_i - \theta_j)\]
   3. \[ e_i f_j - f_i e_j = -|V_i||V_j|sin(\theta_i - \theta_j)\]

  whit a change of variables we can introduce the alternative formulation of the
  OPF problem as follows:

   \[ \min_{P_g} \sum_{i=1}^N C(P_i^g) \]
   s.t
   \[ P_i^g - P_i^d = G_{ii} C_{ii} + \sum_{j \in \delta(i)}(
   G_{ij}C_{ij} -B_{ij} S_{ij}) \]

    \[ Q_i^g - Q_i^d = -B_{ii}C_{ii} + \sum_{j \in \delta(i)}(
   -B_{ij}C_{ij} -G_{ij}S_{ij}) \]

   \[ V_{min}^2 \leq C_{ii} \leq V_{max}^2\]

   \[ P_i^{min} \leq P_i^g \leq P_i^{max} \]

   \[ Q_i^{min} \leq Q_i^g \leq Q_i^{max} \]

   \[ C_{ij} = C_{ji} \]

   \[ S_{ij} = -S{ji} \]

   \[ C_{ij}^2 + S_{ij}^2 = C_{ii} C_{jj} \]

   we can recover the voltage phase angles \theta_{i}'s by solving the following
   system of linear equations with the optimal solution C_{ij}, S_{ij}:

   \[ \theta_j - \theta_i = atan2(S_{ij},C_{ij}) \quad (i,j) \in lines\]

   this works for radial networks.

   the restriction \[ C_{ij}^2 + S_{ij}^2 = C_{ii} C_{jj} \] is nonconvex for
   that reason is used the follow SOCP relaxation:
   \[ C_{ij}^2 + S_{ij}^2 \leq C_{ii} C_{jj} \] which can be rewrite as follow:
   \[ C_{ij}^2 + S_{ij}^2 + \left(\frac{C_{ii} - C_{jj}}{2}\right)^2 \leq
   \left(\frac{C_{ii} + C_{jj}}{2}\right)^2 \]

   so the optimization problem with the variables changes and SOCP relaxation is
   the following:

   \[ \min_{P_g} \sum_{i=1}^N C(P_i^g) \]
   s.t
   \[ P_i^g - P_i^d = G_{ii} C_{ii} + \sum_{j \in \delta(i)}(
   G_{ij}C_{ij} -B_{ij} S_{ij}) \]

    \[ Q_i^g - Q_i^d = -B_{ii}C_{ii} + \sum_{j \in \delta(i)}(
   -B_{ij}C_{ij} -G_{ij}S_{ij}) \]

   \[ V_{min}^2 \leq C_{ii} \leq V_{max}^2\]

   \[ P_i^{min} \leq P_i^g \leq P_i^{max} \]

   \[ Q_i^{min} \leq Q_i^g \leq Q_i^{max} \]

   \[ C_{ij} = C_{ji} \]

   \[ S_{ij} = -S{ji} \]

   \[ C_{ij}^2 + S_{ij}^2 + \left(\frac{C_{ii} - C_{jj}}{2}\right)^2 \leq
   \left(\frac{C_{ii} + C_{jj}}{2}\right)^2 \]

   that problem can be solved given all information of each node, but we need
   to solve the problem in a distributed fashion where each node compute its
   variables with only data of its neighbors and finally a global solution
   are finded, but we need to rewrite the problem based on Consensus and ADMM

*** Global Consensus algorithm
    the main idea in solving the OPF in a distributed fashion is that each node
    can find its optimal variables and for that it require information from its
    neighbors and at the same time somw variables in adjacent nodes must be the
    same so in this situation we need a Consensus between adjacent nodes, so the
    variables in each node are compose by local variables (\(\overline{x_i}\)) and
    coupling variables (\(\widetilde{x_i} \)). local variables are no shared with
    other nodes but coupling variables are shared and need to be the same so for
    coupling variables a consensus need to be defined finally the optimization
    variables can be expressed as:

    \[ X_i = \overline{x_i} + \widetilde{x_i}\]

    here \(\overline{x_i}\) is compose by generation power P_i^g and Q_i^g and
    \( \widetilde{x_i}\) is compose by variables boundary variables C_{ii},
    C_{ij}, and S_{ij}.
    for a general structure of \(\widetilde{x_i}\) it is suppose that each node
    have a parent and zero, one or more child,

    \begin{figure}[h!]
    \begin{center}
    \begin{tikzpicture}
    \draw (-4,0) node [anchor=east]{Node p} -- (-1,0);
    \draw (0,0) node {Node m} circle (1);
    \draw (1,0) -- (4,0) node [anchor=west]{Node n};
    \end{tikzpicture}
    \caption{example of variables \(\widetilde{X}\) in node m}
    \end{center}
    \end{figure}

    \( \widetilde{X_m} = [C_{mm}, C_{pp}, C_{nn}, C_{mp}, C_{mn}, S_{mp}, S_{mn}]\)

    \(\newline\)

    The pattern is as follows the first position is always the var C_{ii}, of local node
    the second position is always the var C_{ii} of parent node and the rest of N
    positions is the corresponding variables C_{ii} of each children, next we have
    the variable C_{ip} that  correspond to variable C_{ij} from node i to node parent
    the next N position is filled with the variable C_{ij} from node i to node child j
    and last we have the variable S_{ip} that correspond to variable S_{ij} from node i
    to node father j and the rest of N positions correspond to variables S_{ij} from
    node i to child j.


    The variables \( \widetilde{X_i}\) lives in an space of solution composed by
    all restriction of the OPF, that is the limits of voltages, generation power and
    power balance

    so we can rewrite the main problem as follows:


    \( \min \sum_{i \in N} C_i(X_i)\)

    s.t

    \( X_i \in \chi \)

    \( \widetilde{x_i} - z_i = 0\)

    the objective function can be solved by each node and the complete solution
    is the sum of all individual solutions.

    the previous formulation are in the general consensus form and can be solved
    through ADMM as follow:

    \(\min_{x,z} \mathcal{L}(x,z,\lambda) = \sum_{i \in N}\left[C_i(X_i) +
    \lambda^T(\widetilde{x_i}-z_i)+(\rho/2)|| \widetilde{x_i} - z_n||\right]
    \)

    \( x_i^{k+1} = argmin_{x_i \in \chi }\left(Ci(x_i) + \lambda^{kT}\widetilde{x_i} + (\rho / 2) |||\widetilde{x_i}-z_i^k| \rigth) \)
