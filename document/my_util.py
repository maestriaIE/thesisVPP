import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def change_risk_behavior():
    df = pd.read_csv('../VPP_real_time_op/day-ahead/variandoGamma.csv')
    fobj = df.Obj
    rho = df.Risk
    data = {'rho': rho, 'fobj': fobj}
    data_df = pd.DataFrame(data)
    data_df.to_csv('./images/sources/risk_day_ahead.csv', index=False)

    fig, ax = plt.subplots()
    ax.plot(rho, fobj)
    plt.show()


def getDieselCost(Pdiesel, C):
    diesel_lt_cost = 2271.87
    x = diesel_lt_cost * (1.38e-2 * Pdiesel**2 + 228.04 * Pdiesel)
    return C * x


def print_cost_info(Ppv_da, Ppv_rt, Pmg, Pdiesel, Pvpp_da, Pvpp_rt, Cost_rt):
    C_dso_d = 1.5
    C_dso = 50000
    cost_diesel_gen = getDieselCost(Pdiesel.sum(), C_dso_d)
    cost_PV_gen = C_dso * (Ppv_rt.sum() + Pmg.sum())
    total_gen_cost = cost_diesel_gen + cost_PV_gen
    err_pv = np.abs(Ppv_da - Ppv_rt)
    err_vpp = Pvpp_da - Pvpp_rt
    PcostInMarket = np.dot(Cost_rt, Pvpp_rt)

    print('Total P offerted : ', Pvpp_da.sum())
    print('Total P generated: ', Pvpp_rt.sum())
    print('Error in Pvpp gen: ', err_vpp.sum())

    print('\nPV. MG gen cost : ', cost_PV_gen)
    print('diesel gen Cost   : ', cost_diesel_gen)
    print('Total gen Cost    : ', total_gen_cost)
    print('Total Cost received from market:', PcostInMarket)
    print('total Profit      :', PcostInMarket - total_gen_cost)


def run_test(dayAheadFile, rad_var_file, cost_var_file, rt_out_file,
             TotalAreaPV):
    # dataframe for radiation
    Ppv_df = pd.read_csv(rad_var_file)
    Cost_df = pd.read_csv(cost_var_file)

    da_df = pd.read_csv(dayAheadFile)

    # dataframe for Pvpp
    # rt_df = pd.read_csv("./images/sources/rto_sc1.csv")

    # convert radiation power
    areaTotal = TotalAreaPV
    # Ppv_da = Ppv_df['Rda'] * areaTotal * (1 / 1000000)
    # Ppv_rt = Ppv_df['Rrt'] * areaTotal * (1 / 1000000)
    Ppv_da = da_df['Ppv_da']
    Ppv_rt = da_df['Ppv_rt']

    error_pv = np.abs(Ppv_da - Ppv_rt)

    error_Pvpp = np.abs(da_df['Pvpp'] - da_df['Pvpp_rt'])
    time = np.arange(1, 25)
    Pmg = da_df['Pvpp_rt'] - (Ppv_rt + da_df['Pst_rt'] + da_df['Pdiesel_rt'])

    # Pmg = np.array([
    #     0.010, 0.05, 0.098, 0.04, 0.26, 0.16, 0.1031, 0.075, 0.20, 0.096,
    #     0.052, 0.011, 0.1549, 0.169, 0.0753, 0.24, 0.2399, 0.267, 0.25389,
    #     0.269, 0.2164, 0.207928, 0.18, 0.09
    # ])

    rt = {
        'time': time,
        'Pvpp_da': da_df['Pvpp'],
        'Pvpp_rt': da_df['Pvpp_rt'],
        'Ppv_da': Ppv_da,
        'Ppv_rt': Ppv_rt,
        'error_pv': error_pv,
        'error_Pvpp': error_Pvpp,
        'Pst': da_df['Pst_rt'],
        'Pmg': Pmg,
        'Pdiesel_rt': da_df['Pdiesel_rt'],
        'Cost_rt': Cost_df['Cost_rt'],
        'Cost_da': Cost_df['Cost_da'],
    }
    fig, axis = plt.subplots(5, 1)
    axis[0].plot(time, da_df['Pvpp'], label='Pvpp_da')
    axis[0].plot(time, da_df['Pvpp_rt'], label='Pvpp_rt')
    axis[0].legend()

    axis[1].plot(time, Pmg, label='MG')
    axis[1].plot(time, Ppv_da, label='Ppv_da')
    axis[1].plot(time, Ppv_rt, label='Ppv_rt')
    axis[1].legend()

    axis[2].plot(time, da_df['Pst_rt'], label='Pst')
    axis[2].legend()

    axis[3].plot(time, da_df['Pdiesel_rt'], label='Pdiesel')
    axis[3].legend()

    axis[4].stem(time, error_Pvpp, label='error Pvpp')
    axis[4].legend()

    # axis[5].stem(time, error_pv, label='error Ppv')
    # axis[5].legend()

    plt.show()

    rt_df = pd.DataFrame(rt)
    rt_df.to_csv(rt_out_file, index=False)
    print_cost_info(Ppv_da, Ppv_rt, Pmg, da_df['Pdiesel_rt'], da_df['Pvpp'],
                    da_df['Pvpp_rt'], Cost_df['Cost_rt'])


def test1():
    print('case 1')
    areaTotal_PV = 900
    input_da_file = "./images/sources/day_ahead_sc1.csv"
    rad_var_file = "./images/sources/rad_variation_sc1.csv"
    cost_var_file = "./images/sources/cost_variation_sc1.csv"
    rt_out_file = "./images/sources/rto_sc1.csv"
    run_test(input_da_file, rad_var_file, cost_var_file, rt_out_file,
             areaTotal_PV)


def test2():
    print('case 2')
    areaTotal_PV = 3000
    input_da_file = "./images/sources/day_ahead_sc2.csv"
    rad_var_file = "./images/sources/rad_variation_sc2.csv"
    cost_var_file = "./images/sources/cost_variation_sc2.csv"
    rt_out_file = "./images/sources/rto_sc2.csv"
    run_test(input_da_file, rad_var_file, cost_var_file, rt_out_file,
             areaTotal_PV)


def test3():
    print('case 3')
    areaTotal_PV = 6000
    input_da_file = "./images/sources/day_ahead_sc3.csv"
    rad_var_file = "./images/sources/rad_variation_sc3.csv"
    cost_var_file = "./images/sources/cost_variation_sc3.csv"
    rt_out_file = "./images/sources/rto_sc3.csv"
    run_test(input_da_file, rad_var_file, cost_var_file, rt_out_file,
             areaTotal_PV)


if __name__ == "__main__":
    #test1()
    #    print('Diesel Cost', getDieselCost(0.149, 1.5))
    #exit()
    test3()
