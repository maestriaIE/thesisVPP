import cvxpy as cvx
import numpy as np
import pandas as pd
import util


def solve(Cost,
          Pgmax_slack,
          Pgmin_slack,
          Qgmax_slack,
          Qgmin_slack,
          G,
          B,
          N,
          lines,
          Pd,
          Qd,
          Vmin,
          Vmax,
          rho=100):

    # variables
    P = cvx.Variable(N)
    Q = cvx.Variable(N)
    Pslack = cvx.Variable(N)
    Qslack = cvx.Variable(N)

    # variables for SOC relaxation
    C = cvx.Symmetric(N)
    S = cvx.Variable(N, N)

    # Costo de generacion slack
    cost_pg = cvx.sum_entries(Cost * P)

    # costo de generacion paneles solares
    # pv_cost = cvx.sum_entries((0.8 * Cost) * Ppv)

    obj = cvx.Minimize(cost_pg)

    global_constraints = [
        P == Pslack,
        Q == Qslack,
        # slack
        Pslack <= Pgmax_slack,
        Pslack >= Pgmin_slack,
        Qslack <= Qgmax_slack,
        Qslack >= Qgmin_slack,
        C[0, 0] == 1,
        # Sii does not exit
        cvx.diag(S) == np.zeros(N),

        # antisimetric matrix
        S == -S.T,

        # power balance
        P == cvx.diag(G * C) - cvx.diag(B * S.T) + Pd,
        Q == -cvx.diag(B * C) - cvx.diag(G * S.T) + Qd,

        # vmin^2 <= cii <= vmax^2
        cvx.diag(C) <= np.ones(N) * Vmax**2,
        cvx.diag(C) >= np.ones(N) * Vmin**2
    ]

    # SOCP restrictions
    print('lines:', lines)
    for i, j in lines:
        i = i - 1
        j = j - 1
        lhs_i = cvx.vstack(C[i, j], S[i, j], ((C[i, i] - C[j, j]) / 2))
        rhs_i = (C[i, i] + C[j, j]) / 2

        lhs_j = cvx.vstack(C[j, i], S[j, i], ((C[j, j] - C[i, i]) / 2))
        rhs_j = (C[j, j] + C[i, i]) / 2

        global_constraints.append(cvx.norm(lhs_i) <= rhs_i)
        global_constraints.append(cvx.norm(lhs_j) <= rhs_j)

    problem = cvx.Problem(obj, global_constraints)
    try:
        problem.solve(solver='MOSEK', verbose=False)
    except:
        problem.solve(solver='MOSEK', verbose=True)
        return "Error", []
    #   problem.solve(solver='MOSEK', verbose=True)

    # print('Cost:\n', Cost)
    print('Status:', problem.status)
    print('Fobj:', problem.value)
    print('Pd:', Pd)
    print('P:', Pslack.value)
    print('Pslack:', np.sum(Pslack.value))
    print('Pd:', np.sum(Pd))
    exit()
    return "Ok", problem.value, Pvpp.value.flatten().tolist()[0]
    # theta = util.get_thetas(lines, C.value, S.value)
    # V = util.get_voltages(theta, C.value)
    # Pg = np.array(P.value.ravel().tolist()[0])
    # Qg = np.array(Q.value.ravel().tolist()[0])
    # return V, Pg - Pd, Qg - Qd, S.value, C.value


if __name__ == "__main__":
    testcase = './input/in3.xlsx'
    data = util.getData(testcase)
    N = len(data['lines']) + 1

    rho = 1e3

    # spot energy market prices
    Cost = 200000
    # demand
    Pd = np.array(data['Pd'])
    Qd = np.array(data['Qd'])

    G = data['G']
    B = data['B']
    lines = data['lines']
    Pgmax_slack = data['Pg_max_slack']
    Pgmin_slack = data['Pg_min_slack']
    Qgmax_slack = data['Qg_max_slack']
    Qgmin_slack = data['Qg_min_slack']
    Vmin = data['Vmin']
    Vmax = data['Vmax']

    # print('Est_init:\n', Est_init)
    # print('Est_end:\n', Est_end)
    # print('Pst_max:\n', Pst_max)
    # print('Sst_max:\n', Sst_max)
    # print('Pgmax_slack:\n', Pgmax_slack)
    # print('Pgmin_slack:\n', Pgmin_slack)
    # print('Qgmax_slack:\n', Qgmax_slack)
    # print('Qgmin_slack:\n', Qgmin_slack)
    # print('Pdiesel_max:\n', Pdiesel_max)
    # print('Sdiesel_max:\n', Sdiesel_max)
    # print('diesel_a_param:\n', diesel_a_param)
    # print('diesel_b_param:\n', diesel_b_param)
    # print('lt_diesel_cost:\n', lt_diesel_cost)

    # print('radiation:\n', radiation)
    # print('Ppv_max_fixed:', Ppv_max_fixed)
    # print('Spv_max:\n', Spv_max)
    # print('pv_nodes:', pv_nodes)
    # print('pv_area:', pv_area)
    # exit()

    solve(Cost, Pgmax_slack, Pgmin_slack, Qgmax_slack, Qgmin_slack, G, B, N,
          lines, Pd, Qd, Vmin, Vmax, rho)
    exit()
