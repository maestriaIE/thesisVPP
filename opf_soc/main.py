import sys
import os
sys.path.append('./day-ahead')
import math_model as vpp_rt
import pandas as pd
import numpy as np
import util
import matplotlib.pyplot as plt
import time


def solve_system(input_data, testcase, period, tw=4, rho=100):
    data = util.getData(testcase)
    lines = data['lines']
    N = len(lines) + 1
    # external data
    Cost = input_data['Cost']
    Pvpp_da = input_data['Pvpp_da']

    # Batteries
    Est_init = input_data['Est_init']
    Est_end = input_data['Est_end']
    Pst_max = data['Pst_max']
    # Est_max = data['Est_max']
    Sst_max = data['Sst_max']

    # Slack
    Pgmax_slack = data['Pg_max_slack']
    Pgmin_slack = data['Pg_min_slack']
    Qgmax_slack = data['Qg_max_slack']
    Qgmin_slack = data['Qg_min_slack']

    # Diesel
    Pdiesel_max = data['Pdiesel_max']
    Sdiesel_max = data['Sdiesel_max']
    diesel_a_param = data['diesel_a_param']
    diesel_b_param = data['diesel_b_param']
    lt_diesel_cost = data['lt_diesel_cost']

    # Photo-voltaic generation
    radiation = input_data['radiation']
    Ppv_max_fixed = data['Ppv_max']
    Spv_max = data['Spv_max']
    pv_nodes = data['pv_nodes']
    pv_area = data['pv_area']

    # technical characteristics
    G = data['G']
    B = data['B']

    Vmin = data['Vmin']
    Vmax = data['Vmax']

    # demand
    Pd = np.array([input_data['Pd'] for i in range(tw)])
    Qd = np.array([input_data['Qd'] for i in range(tw)])

    # print('periodo:', period)
    # print('Cost:', Cost)
    # print('Pvpp_da', Pvpp_da)
    # print('Est_init:\n', Est_init)
    # print('Est_end:\n', Est_end)
    # print('Pst_max:\n', Pst_max)
    # print('Sst_max:\n', Sst_max)
    # print('Pgmax_slack:\n', Pgmax_slack)
    # print('Pgmin_slack:\n', Pgmin_slack)
    # print('Qgmax_slack:\n', Qgmax_slack)
    # print('Qgmin_slack:\n', Qgmin_slack)
    # print('Pdiesel_max:\n', Pdiesel_max)
    # print('Sdiesel_max:\n', Sdiesel_max)
    # print('diesel_a_param:\n', diesel_a_param)
    # print('diesel_b_param:\n', diesel_b_param)
    # print('lt_diesel_cost:\n', lt_diesel_cost)
    # print('vmin:', Vmin)
    # print('radiation:\n', radiation)
    # print('Ppv_max_fixed:', Ppv_max_fixed)
    # print('Spv_max:\n', Spv_max)
    # print('pv_nodes:', pv_nodes)
    # print('pv_area:', pv_area)

    return vpp_rt.solve(Cost, Pvpp_da, Est_init, Est_end, Pst_max, Sst_max,
                        Pgmax_slack, Pgmin_slack, Qgmax_slack, Qgmin_slack,
                        Pdiesel_max, Sdiesel_max, diesel_a_param,
                        diesel_b_param, lt_diesel_cost, radiation,
                        Ppv_max_fixed, Spv_max, pv_nodes, pv_area, G, B, N,
                        lines, Pd, Qd, Vmin, Vmax, tw, rho)


def plot_data(Pvpp_da, Pvpp_rt):
    x = np.arange(len(Pvpp_da))
    plt.plot(x, Pvpp_da, 'b', label='Pvpp_da')
    plt.plot(x, Pvpp_rt, 'r', label='Pvpp_rt')
    plt.xlabel('time Hours')
    plt.legend(loc='upper left')
    plt.title('Real time Operation of VPP')
    plt.show()


def plot_error(Pvpp_da, Pvpp_rt):
    x = np.arange(len(Pvpp_da))
    Pvpp_da = Pvpp_da.flatten().tolist()[0]
    print(Pvpp_rt)
    print(Pvpp_da)
    err = np.abs(Pvpp_da - Pvpp_rt)
    fig, ax = plt.subplots()
    ax.stem(x, err)
    ax.set_title('Error')
    plt.show()


if __name__ == "__main__":

    external_data_file = './output/external_data.xlsx'
    df_info = pd.read_excel(external_data_file, sheetname='info')
    Rmed_rt = df_info['Rad_med'].values
    Rvar_rt = df_info['Rad_var'].values

    dirname = os.getcwd() + '/'

    # testFeeder system
    #testcase = './input/in69.xlsx'
    testcase = './input/in3.xlsx'
    # num of nodes
    N = 3

    input_day_ahead = dirname + './input/dayAhead_params.xlsx'
    output_file = dirname + './output/dayAhead_output.xlsx'

    # Solve dayAhead
    Pvpp_da, Rmed_da, Rvar_da = pda.getDayAhead(input_day_ahead, output_file)

    Rda = np.abs(np.random.normal(Rmed_da, Rvar_da))
    Rrt = np.abs(np.random.normal(Rmed_rt, Rvar_rt))
    ''' plot difference between predicted and real solar irradiation
    x1 = np.arange(len(Rda))
    plt.plot(x1, Rda, 'b', label='Rad real time')
    plt.plot(x1, Rrt, 'r', label='Rad day ahead')
    plt.xlabel('time Hours')
    plt.legend(loc='upper left')
    plt.show()
    '''
    time_window = 1
    rho = 1e6
    ext_data = {}
    Pvpp_rt = []
    Rad = []
    tmp = 24
    Fobj = []
    for period in range(6, 7):
        while (True):
            # get input data
            input_data = util.get_external_data(ext_data, testcase, period,
                                                time_window)
            # print(input_data)
            start_time = time.time()
            status, value, Pvpp_i = solve_system(input_data, testcase, period,
                                                 time_window, rho)
            if status != "Error":
                # solve_system(input_data, testcase, period)
                Fobj.append(value)
                Pvpp_rt.append(Pvpp_i)
                Rad.append(input_data['radiation'])
                #print('--> Pvpp at time:', period, '  ', Pvpp_i[0], '  ',
                #      (time.time() - start_time), 's')
                print('--> Pvpp at time:', period, '  ', Pvpp_i, '  ',
                      (time.time() - start_time), 's')
                # print("--- %s seconds ---" % (time.time() - start_time))
                # print('Pvpp_rt:', Pvpp_i)
                break

    Pvpp_rt = np.array(Pvpp_rt).T
    #print('Pvpp_rt:', Pvpp_rt[0])
    print('Fobj:', Fobj)
    # print('Pvpp_rt:\n', Pvpp_rt[0])
    # print('Pvpp_da:\n', Pvpp_da[0:tmp])
    #  print('Error:\n', abs(Pvpp_rt[0][0] - Pvpp_da[0:tmp]))
#    plot_data(Pvpp_da[0:tmp], Pvpp_rt[0])
#    plot_error(Pvpp_da[0:tmp], Pvpp_rt[0])
