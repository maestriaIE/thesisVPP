#!/usr/bin/python
import numpy as np
import pandas as pd
from math import atan2, sqrt, sin, cos
from numpy.linalg import inv


def get_thetas(lines, C, S):
    '''
    given new variables Cii, Cij, Sij it return the angles in each nodes
    in the following sense:
    \theta_j - \theta_i = atan2(S_{i,j},C_{i,j})

    Parameters
    ----------
    lines: list of tuples describing lines [(i,j)]
    C    : 2D array with variables related to Cij (float)
    S    : 2D array with variables related to Sij (float)

    Returns
    ---------
    theta: numpy 1D array with angle of each node
    '''

    N = len(lines) + 1
    A = np.zeros((N, N))
    B = np.zeros(N)

    A[0, 0] = 1
    B[0] = 0

    for row, (i, j) in enumerate(lines):
        A[row + 1, i - 1] = -1
        A[row + 1, j - 1] = 1
        B[row + 1] = atan2(S[i - 1, j - 1], C[i - 1, j - 1])

    theta = np.linalg.solve(A, B)
    return theta


def get_voltages(theta, C):
    '''
    Get Voltages in complex form indicating Voltage magnitude and angle

    Parameters
    ----------
    theta : numpy 1D array with nodes theta
    C     : 2D array related to variables Cij (float)

    Returns
    ---------
    V : complex numpy 1D array with nodal voltage
    '''

    N = len(theta)
    V = np.zeros(N, dtype=complex)
    for i in range(0, N):
        v = sqrt(C[i, i])
        V[i] = complex(v * cos(theta[i]), v * sin(theta[i]))
    return V


def get_Ybus(data):
    '''
    Compute Ybus matriz from system data

    Parameters
    ----------
    Data : pandas dataFrame with info

    Returns
    ----------
    Ybus: numpy 2D complex array,
    SP,SI,SZ: numpy 1D complex array
    '''

    NL = len(data)
    NN = NL + 1

    Ybus = np.complex_(np.zeros([NN, NN]))  # initialize YBUS
    SP = np.complex_(np.zeros([NN]))  # to save constant power loads
    SZ = np.complex_(np.zeros([NN]))  # to save constant impedance loads
    SI = np.complex_(np.zeros([NN]))  # to save constant current loads

    for index, row in data.iterrows():
        N1 = int(row['Node1']) - 1
        N2 = int(row['Node2']) - 1
        ykm = 1 / complex(row['rkm'], row['xkm'])
        bkm = complex(0, row['bkm/2'])
        Ybus[N1, N1] += ykm + bkm
        Ybus[N1, N2] -= ykm
        Ybus[N2, N1] -= ykm
        Ybus[N2, N2] += ykm + bkm
        alfa = row['alpha']
        S = complex(row['Pd'], row['Qd'])
        if alfa == 0:
            SP[N2] = -S
        elif alfa == 1:
            SI[N2] = -S
        elif alfa == 2:
            SZ[N2] = -S
    return Ybus, SP, SI, SZ


def loadFlow(Ybus, SP, SI, SZ):
    '''
    Compute the load flow of system through Quadractic aproximation made by
    Alejandro Garces

    Parameters
    ----------
    Ybus : numpy 2D complex array
    SP,SI,SZ:  numpy 1D complex array

    Returns
    ---------
    Vs : numpy complex 1D array
    MM : ?
    A: ?
    '''
    NN = len(Ybus)
    NL = NN - 1
    YNS = Ybus[0, 1:]  # (vector of the Ybus related to the slack)
    YNN = Ybus[1:, 1:]  # (vector of the Ybus related to other nodes)

    A = YNS - 2 * np.conj(SP[1:]) - np.conj(SI[1:])
    B = np.diag(np.conj(SP[1:]))
    C = YNN - np.diag(np.conj(SZ[1:]))

    # print('A',len(A),'B',len(B),'x',len(B[0]),'C',len(C))
    MM = np.zeros((2 * (NN - 1), 2 * (NN - 1)))
    MM[:NL, :NL] = np.real(B + C)
    MM[:NL, NL:] = np.imag(B - C)
    MM[NL:, :NL] = np.imag(B + C)
    MM[NL:, NL:] = np.real(C - B)

    tmp = np.zeros(2 * NL)
    tmp[:NL] = -np.real(A)
    tmp[NL:] = -np.imag(A)
    VRI = inv(MM).dot(tmp)
    Vs = np.complex_(np.zeros(NN))
    Vs[0] = 1
    # print('VRI:',len(VRI))
    Vs[1:] = np.complex_(list(map(complex, VRI[:NL], VRI[NL:])))
    return Vs, MM, A


def getData(testcase):
    # print(testcase)
    # load dataframes with feeder data
    system_df = pd.read_excel(testcase, sheetname='system')
    param_df = pd.read_excel(testcase, sheetname='parameters')

    #print(pv_df)

    # get lines of power system
    lines = list(zip(system_df['Node1'], system_df['Node2']))
    N = len(lines) + 1
    #
    Ybus, SP, SI, SZ = get_Ybus(system_df)
    G = np.real(Ybus)  # matriz conductancia
    B = np.imag(Ybus)  # matriz susceptancia

    Vmin = 0.9
    Vmax = 1.1

    # load Slack data
    slack_df = pd.read_excel(testcase, sheetname='slack')
    Pg_max_slack = np.zeros(N)
    Pg_max_slack[0] = slack_df['Pg_max_slack'][0]
    Pg_min_slack = np.zeros(N)
    Pg_min_slack[0] = slack_df['Pg_min_slack'][0]
    Qg_max_slack = np.zeros(N)
    Qg_max_slack[0] = slack_df['Qg_max_slack'][0]
    Qg_min_slack = np.zeros(N)
    Qg_min_slack[0] = slack_df['Qg_min_slack'][0]

    data = {
        'lines': lines,
        'Ybus': Ybus,
        'G': G,
        'B': B,
        'Pd': param_df['Pd'].values,
        'Qd': param_df['Qd'].values,
        'Pg_max_slack': Pg_max_slack,
        'Pg_min_slack': Pg_min_slack,
        'Qg_max_slack': Qg_max_slack,
        'Qg_min_slack': Qg_min_slack,
        'Vmin': Vmin,
        'Vmax': Vmax
    }

    return data


def get_external_data(data, testcase, period, tw=4):
    '''
    return market cost, Est_init and radiation data for next tw hours
    '''
    external_data_file = './output/ed.xlsx'
    df_system = pd.read_excel(testcase, sheetname='parameters')
    N = len(df_system['Qd'].values)
    # generar en un xlsx Pvpp la radiacion y el costo en tw por cada ejecucion
    if 'Pvpp_da' not in data:
        df_Pvpp = pd.read_excel(external_data_file, sheetname='Pvpp')
        df_info = pd.read_excel(external_data_file, sheetname='info')
        df_Est = pd.read_excel(external_data_file, sheetname='Est')
        data['Pvpp_da'] = df_Pvpp['Pvpp'].values
        data['Cost_med'] = df_info['Cost_med'].values
        data['Cost_var'] = df_info['Cost_var'].values
        data['Rad_med'] = df_info['Rad_med'].values
        data['Rad_var'] = df_info['Rad_var'].values
        data['Est_da'] = df_Est.values
        data['Est_da_nodes'] = df_Est.columns.values

    obj = {}

    if (period > 20):
        first = data['Cost_med'][period:]
        end = data['Cost_med'][:(4 - (24 - period))]
        cost_med = np.append(first, end)

        first = data['Cost_var'][period:]
        end = data['Cost_var'][:(4 - (24 - period))]
        cost_var = np.append(first, end)

        first = data['Pvpp_da'][period:]
        end = data['Pvpp_da'][:(4 - (24 - period))]
        Pvpp_da = np.append(first, end)

        first = data['Rad_med'][period:]
        end = data['Rad_med'][:(4 - (24 - period))]
        rad_med = np.append(first, end)

        first = data['Rad_var'][period:]
        end = data['Rad_var'][:(4 - (24 - period))]
        rad_var = np.append(first, end)

    else:
        # se generan los costos en la ventana de tiempo
        cost_med = data['Cost_med'][period:period + tw]
        cost_var = data['Cost_var'][period:period + tw]

        Pvpp_da = data['Pvpp_da'][period:period + tw]
        rad_med = data['Rad_med'][period:period + tw]
        rad_var = data['Rad_var'][period:period + tw]

    #obj['Cost'] = np.random.normal(cost_med, cost_var)
    obj['Cost'] = cost_med * 1000

    # se obtiene la Pvpp programada para la ventana de tiempo
    obj['Pvpp_da'] = Pvpp_da

    # se genera la radiacion para la ventana de tiempo
    obj['radiation'] = np.abs(np.random.normal(rad_med, rad_var))
    # obj['radiation'] = rad_med
    # print('rad_med:', rad_med)
    # print('rad_var:', rad_var)
    # print('Radiation:', obj['radiation'])
    # se obtiene la Energia incial de las baterias para tw
    Est_init_med = data['Est_da'][period]
    Est_var = 10 * np.ones(len(data['Est_da'][period]))
    Est_init_val = np.random.normal(Est_init_med, Est_var) / 1000
    Est_init = np.zeros(N)

    # Est_init[data['Est_da_nodes'] - 1] = Est_init_val
    Est_init[data['Est_da_nodes'] - 1] = Est_init_med / 1000

    obj['Est_init'] = Est_init

    # se obtiene la Energia de las baterias al final de tw
    Est_end = np.zeros(N)
    Est_end[data['Est_da_nodes'] - 1] = data['Est_da'][(period + tw - 1) % 24]
    obj['Est_end'] = Est_end / 1000
    # print('period:', period)
    # print('period + tw:', period + tw)
    # print('Est_end', Est_end / 1000)
    # Potencias de carga - falta generarlas dinamicamente
    obj['Pd'] = df_system['Pd'].values
    obj['Qd'] = df_system['Qd'].values

    return obj


def print_variables(lines, C, S):
    '''
    Print Variables Cii, Cij, Sij acording list of lines
    '''
    for i, j in lines:
        i = i - 1
        j = j - 1
        print('line (i,j): ', i + 1, j + 1)
        print('Cii:', C[i, i], 'Cij:', C[i, j], ' Sij', S[i, j])


if __name__ == "__main__":
    # get input data
    testcase = 'input/in69.xlsx'
    df = pd.read_excel(testcase, sheetname='system')

    # get Ybus
    Ybus, SP, SI, SZ = get_Ybus(df)

    # get voltage solutions (given nodal power)
    Vs, MM, A = loadFlow(Ybus, SP, SI, SZ)
